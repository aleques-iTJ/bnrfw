﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bnrToolkit
{
    public class FarewellStrings
    {
        public static List<string> farewells;

        static FarewellStrings()
        {
            farewells = new List<string>();

            farewells.Add("The Embiggening Cometh.");
            farewells.Add("Harder than ever.");
            farewells.Add("NOT NOW MOM, I'M BONER FRAMEWORKING");
            farewells.Add("Fedora mites?");
            farewells.Add("What the heck is a neck-beard error?");
            farewells.Add("A giant... WORM?");
	    farewells.Add("Overclock my WHAT?");
            farewells.Add("*snort* Nice boat, Dad.");
            farewells.Add("Of course a katana could cut through it you dolt.");
            farewells.Add("N-Sync was such an amazing band...");
            farewells.Add("My Dad's a time cop.");
            farewells.Add("Now Entering: FUN ZONE");
            farewells.Add("What do you mean I'm banned from the buffet?");
            farewells.Add("This Beggin' Strips brand jerky is really good, man.");
        }

        public static string GetRandomString()
        {
            Random rnd = new Random();
            return farewells.ElementAt(rnd.Next(farewells.Count));
        }
    }
}

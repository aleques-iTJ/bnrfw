﻿namespace bnrToolkit
{
    partial class frmTexture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExport = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.txtTextureInfo = new System.Windows.Forms.TextBox();
            this.picTexture = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkCompress = new System.Windows.Forms.CheckBox();
            this.cbxFilterType = new System.Windows.Forms.ComboBox();
            this.chkGenMips = new System.Windows.Forms.CheckBox();
            this.cbxFormat = new System.Windows.Forms.ComboBox();
            this.cbxMipmapResampleType = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.chkAnimated = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.txtMipNum = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picTexture)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExport
            // 
            this.btnExport.Enabled = false;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.Location = new System.Drawing.Point(98, 12);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(80, 20);
            this.btnExport.TabIndex = 45;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnImport
            // 
            this.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImport.Location = new System.Drawing.Point(12, 12);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(80, 20);
            this.btnImport.TabIndex = 43;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // txtTextureInfo
            // 
            this.txtTextureInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTextureInfo.Location = new System.Drawing.Point(12, 38);
            this.txtTextureInfo.Multiline = true;
            this.txtTextureInfo.Name = "txtTextureInfo";
            this.txtTextureInfo.ReadOnly = true;
            this.txtTextureInfo.Size = new System.Drawing.Size(166, 173);
            this.txtTextureInfo.TabIndex = 47;
            // 
            // picTexture
            // 
            this.picTexture.Location = new System.Drawing.Point(0, 0);
            this.picTexture.Name = "picTexture";
            this.picTexture.Size = new System.Drawing.Size(256, 384);
            this.picTexture.TabIndex = 48;
            this.picTexture.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.picTexture);
            this.panel1.Location = new System.Drawing.Point(184, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(256, 384);
            this.panel1.TabIndex = 49;
            // 
            // chkCompress
            // 
            this.chkCompress.AutoSize = true;
            this.chkCompress.Location = new System.Drawing.Point(6, 19);
            this.chkCompress.Name = "chkCompress";
            this.chkCompress.Size = new System.Drawing.Size(139, 17);
            this.chkCompress.TabIndex = 63;
            this.chkCompress.Text = "Compress Texture (LZ4)";
            this.chkCompress.UseVisualStyleBackColor = true;
            this.chkCompress.CheckedChanged += new System.EventHandler(this.chkCompress_CheckedChanged);
            // 
            // cbxFilterType
            // 
            this.cbxFilterType.FormattingEnabled = true;
            this.cbxFilterType.Items.AddRange(new object[] {
            "Nearest",
            "Trilinear",
            "Anisotropic"});
            this.cbxFilterType.Location = new System.Drawing.Point(6, 19);
            this.cbxFilterType.Name = "cbxFilterType";
            this.cbxFilterType.Size = new System.Drawing.Size(154, 21);
            this.cbxFilterType.TabIndex = 61;
            this.cbxFilterType.Text = "Filtering Type";
            this.cbxFilterType.SelectedIndexChanged += new System.EventHandler(this.cbxFilterType_SelectedIndexChanged);
            // 
            // chkGenMips
            // 
            this.chkGenMips.AutoSize = true;
            this.chkGenMips.Location = new System.Drawing.Point(6, 42);
            this.chkGenMips.Name = "chkGenMips";
            this.chkGenMips.Size = new System.Drawing.Size(154, 17);
            this.chkGenMips.TabIndex = 62;
            this.chkGenMips.Text = "Generate Texture Mipmaps";
            this.chkGenMips.UseVisualStyleBackColor = true;
            this.chkGenMips.CheckedChanged += new System.EventHandler(this.chkGenMips_CheckedChanged);
            // 
            // cbxFormat
            // 
            this.cbxFormat.FormattingEnabled = true;
            this.cbxFormat.Items.AddRange(new object[] {
            "RGBA",
            "BGRA",
            "DXT1",
            "DXT3",
            "DXT5"});
            this.cbxFormat.Location = new System.Drawing.Point(6, 46);
            this.cbxFormat.Name = "cbxFormat";
            this.cbxFormat.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbxFormat.Size = new System.Drawing.Size(154, 21);
            this.cbxFormat.TabIndex = 60;
            this.cbxFormat.Text = "Internal Format";
            this.cbxFormat.SelectedIndexChanged += new System.EventHandler(this.cbxFormat_SelectedIndexChanged);
            // 
            // cbxMipmapResampleType
            // 
            this.cbxMipmapResampleType.FormattingEnabled = true;
            this.cbxMipmapResampleType.Items.AddRange(new object[] {
            "Nearest",
            "Bilinear",
            "Bicubic"});
            this.cbxMipmapResampleType.Location = new System.Drawing.Point(6, 65);
            this.cbxMipmapResampleType.Name = "cbxMipmapResampleType";
            this.cbxMipmapResampleType.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbxMipmapResampleType.Size = new System.Drawing.Size(154, 21);
            this.cbxMipmapResampleType.TabIndex = 64;
            this.cbxMipmapResampleType.Text = "Mipmap Resampling Type";
            this.cbxMipmapResampleType.SelectedIndexChanged += new System.EventHandler(this.cbxMipmapResampleType_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkCompress);
            this.groupBox1.Controls.Add(this.chkGenMips);
            this.groupBox1.Controls.Add(this.cbxMipmapResampleType);
            this.groupBox1.Location = new System.Drawing.Point(12, 217);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(166, 93);
            this.groupBox1.TabIndex = 66;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Preprocessing";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.textBox4);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.chkAnimated);
            this.groupBox2.Controls.Add(this.cbxFilterType);
            this.groupBox2.Controls.Add(this.cbxFormat);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Location = new System.Drawing.Point(12, 316);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(166, 148);
            this.groupBox2.TabIndex = 67;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Settings";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 72;
            this.label2.Text = "Frame Rate";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 71;
            this.label1.Text = "Frame Size";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(86, 96);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(74, 20);
            this.textBox4.TabIndex = 70;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(86, 122);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(74, 20);
            this.textBox2.TabIndex = 69;
            // 
            // chkAnimated
            // 
            this.chkAnimated.AutoSize = true;
            this.chkAnimated.Location = new System.Drawing.Point(6, 73);
            this.chkAnimated.Name = "chkAnimated";
            this.chkAnimated.Size = new System.Drawing.Size(151, 17);
            this.chkAnimated.TabIndex = 68;
            this.chkAnimated.Text = "Treat as Animated Texture";
            this.chkAnimated.UseVisualStyleBackColor = true;
            this.chkAnimated.CheckedChanged += new System.EventHandler(this.chkAnimated_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.Location = new System.Drawing.Point(259, 14);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(114, 17);
            this.checkBox3.TabIndex = 68;
            this.checkBox3.Text = "Lock Preview Size";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(379, 12);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(60, 20);
            this.textBox3.TabIndex = 70;
            this.textBox3.Text = "256x384";
            // 
            // txtMipNum
            // 
            this.txtMipNum.Enabled = false;
            this.txtMipNum.Location = new System.Drawing.Point(224, 12);
            this.txtMipNum.Multiline = true;
            this.txtMipNum.Name = "txtMipNum";
            this.txtMipNum.Size = new System.Drawing.Size(29, 20);
            this.txtMipNum.TabIndex = 72;
            this.txtMipNum.Text = "0";
            this.txtMipNum.TextChanged += new System.EventHandler(this.txtMipNum_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(184, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 73;
            this.label3.Text = "Mip #";
            // 
            // frmTexture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 502);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtMipNum);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtTextureInfo);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnImport);
            this.MaximizeBox = false;
            this.Name = "frmTexture";
            this.ShowIcon = false;
            this.Text = "SFTBTCCU";
            ((System.ComponentModel.ISupportInitialize)(this.picTexture)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.TextBox txtTextureInfo;
        private System.Windows.Forms.PictureBox picTexture;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkCompress;
        private System.Windows.Forms.ComboBox cbxFilterType;
        private System.Windows.Forms.CheckBox chkGenMips;
        private System.Windows.Forms.ComboBox cbxFormat;
        private System.Windows.Forms.ComboBox cbxMipmapResampleType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.CheckBox chkAnimated;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox txtMipNum;
        private System.Windows.Forms.Label label3;
    }
}
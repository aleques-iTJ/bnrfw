﻿namespace bnrToolkit
{
    partial class frmArchive
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.trvDirectory = new System.Windows.Forms.TreeView();
            this.btnImport = new System.Windows.Forms.Button();
            this.grpArchiveInfo = new System.Windows.Forms.GroupBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.txtRecordSize = new System.Windows.Forms.TextBox();
            this.txtTableSize = new System.Windows.Forms.TextBox();
            this.txtDescriptorSize = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.txtDataSizeC = new System.Windows.Forms.TextBox();
            this.txtDataSizeUC = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.txtTableEntries = new System.Windows.Forms.TextBox();
            this.txtFileCount = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.txtFolderCount = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.txtFileIndex = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.txtWorkingDirectory = new System.Windows.Forms.TextBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.txtTableLength = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.cbxInfoPanelSelection = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.picTexture = new System.Windows.Forms.PictureBox();
            this.grpArchiveInfo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTexture)).BeginInit();
            this.SuspendLayout();
            // 
            // trvDirectory
            // 
            this.trvDirectory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.trvDirectory.CheckBoxes = true;
            this.trvDirectory.HotTracking = true;
            this.trvDirectory.LabelEdit = true;
            this.trvDirectory.Location = new System.Drawing.Point(12, 41);
            this.trvDirectory.Name = "trvDirectory";
            this.trvDirectory.Size = new System.Drawing.Size(302, 200);
            this.trvDirectory.TabIndex = 10;
            // 
            // btnImport
            // 
            this.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImport.Location = new System.Drawing.Point(88, 15);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(46, 20);
            this.btnImport.TabIndex = 12;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // grpArchiveInfo
            // 
            this.grpArchiveInfo.Controls.Add(this.textBox15);
            this.grpArchiveInfo.Controls.Add(this.txtRecordSize);
            this.grpArchiveInfo.Controls.Add(this.txtTableSize);
            this.grpArchiveInfo.Controls.Add(this.txtDescriptorSize);
            this.grpArchiveInfo.Controls.Add(this.textBox10);
            this.grpArchiveInfo.Controls.Add(this.txtDataSizeC);
            this.grpArchiveInfo.Controls.Add(this.txtDataSizeUC);
            this.grpArchiveInfo.Controls.Add(this.textBox3);
            this.grpArchiveInfo.Controls.Add(this.txtTableEntries);
            this.grpArchiveInfo.Controls.Add(this.txtFileCount);
            this.grpArchiveInfo.Controls.Add(this.textBox11);
            this.grpArchiveInfo.Controls.Add(this.txtFolderCount);
            this.grpArchiveInfo.Controls.Add(this.textBox12);
            this.grpArchiveInfo.Controls.Add(this.textBox4);
            this.grpArchiveInfo.Controls.Add(this.textBox8);
            this.grpArchiveInfo.Controls.Add(this.textBox5);
            this.grpArchiveInfo.Location = new System.Drawing.Point(281, 434);
            this.grpArchiveInfo.Name = "grpArchiveInfo";
            this.grpArchiveInfo.Size = new System.Drawing.Size(302, 121);
            this.grpArchiveInfo.TabIndex = 22;
            this.grpArchiveInfo.TabStop = false;
            this.grpArchiveInfo.Text = "Archive Info";
            // 
            // textBox15
            // 
            this.textBox15.BackColor = System.Drawing.SystemColors.Control;
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox15.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox15.Enabled = false;
            this.textBox15.Location = new System.Drawing.Point(6, 97);
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(70, 13);
            this.textBox15.TabIndex = 50;
            this.textBox15.Text = "Record Size";
            // 
            // txtRecordSize
            // 
            this.txtRecordSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRecordSize.Enabled = false;
            this.txtRecordSize.Location = new System.Drawing.Point(82, 95);
            this.txtRecordSize.Name = "txtRecordSize";
            this.txtRecordSize.Size = new System.Drawing.Size(60, 20);
            this.txtRecordSize.TabIndex = 49;
            this.txtRecordSize.Text = "0";
            // 
            // txtTableSize
            // 
            this.txtTableSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTableSize.Enabled = false;
            this.txtTableSize.Location = new System.Drawing.Point(82, 43);
            this.txtTableSize.Name = "txtTableSize";
            this.txtTableSize.Size = new System.Drawing.Size(60, 20);
            this.txtTableSize.TabIndex = 48;
            this.txtTableSize.Text = "0";
            // 
            // txtDescriptorSize
            // 
            this.txtDescriptorSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescriptorSize.Enabled = false;
            this.txtDescriptorSize.Location = new System.Drawing.Point(82, 69);
            this.txtDescriptorSize.Name = "txtDescriptorSize";
            this.txtDescriptorSize.Size = new System.Drawing.Size(60, 20);
            this.txtDescriptorSize.TabIndex = 45;
            this.txtDescriptorSize.Text = "0";
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.SystemColors.Control;
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox10.Enabled = false;
            this.textBox10.Location = new System.Drawing.Point(6, 71);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(70, 13);
            this.textBox10.TabIndex = 47;
            this.textBox10.Text = "Descriptor Size";
            // 
            // txtDataSizeC
            // 
            this.txtDataSizeC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDataSizeC.Enabled = false;
            this.txtDataSizeC.Location = new System.Drawing.Point(234, 95);
            this.txtDataSizeC.Name = "txtDataSizeC";
            this.txtDataSizeC.Size = new System.Drawing.Size(60, 20);
            this.txtDataSizeC.TabIndex = 40;
            this.txtDataSizeC.Text = "0";
            // 
            // txtDataSizeUC
            // 
            this.txtDataSizeUC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDataSizeUC.Enabled = false;
            this.txtDataSizeUC.Location = new System.Drawing.Point(234, 69);
            this.txtDataSizeUC.Name = "txtDataSizeUC";
            this.txtDataSizeUC.Size = new System.Drawing.Size(60, 20);
            this.txtDataSizeUC.TabIndex = 39;
            this.txtDataSizeUC.Text = "0";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Control;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(158, 97);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(70, 13);
            this.textBox3.TabIndex = 42;
            this.textBox3.Text = "Data Size (C)";
            // 
            // txtTableEntries
            // 
            this.txtTableEntries.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTableEntries.Enabled = false;
            this.txtTableEntries.Location = new System.Drawing.Point(82, 17);
            this.txtTableEntries.Name = "txtTableEntries";
            this.txtTableEntries.Size = new System.Drawing.Size(60, 20);
            this.txtTableEntries.TabIndex = 32;
            this.txtTableEntries.Text = "0";
            // 
            // txtFileCount
            // 
            this.txtFileCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFileCount.Enabled = false;
            this.txtFileCount.Location = new System.Drawing.Point(234, 43);
            this.txtFileCount.Name = "txtFileCount";
            this.txtFileCount.Size = new System.Drawing.Size(60, 20);
            this.txtFileCount.TabIndex = 28;
            this.txtFileCount.Text = "0";
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.SystemColors.Control;
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox11.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox11.Enabled = false;
            this.textBox11.Location = new System.Drawing.Point(6, 45);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(70, 13);
            this.textBox11.TabIndex = 46;
            this.textBox11.Text = "Table Size";
            // 
            // txtFolderCount
            // 
            this.txtFolderCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFolderCount.Enabled = false;
            this.txtFolderCount.Location = new System.Drawing.Point(234, 17);
            this.txtFolderCount.Name = "txtFolderCount";
            this.txtFolderCount.Size = new System.Drawing.Size(60, 20);
            this.txtFolderCount.TabIndex = 26;
            this.txtFolderCount.Text = "0";
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.SystemColors.Control;
            this.textBox12.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox12.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox12.Enabled = false;
            this.textBox12.Location = new System.Drawing.Point(6, 19);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(70, 13);
            this.textBox12.TabIndex = 33;
            this.textBox12.Text = "Table Entries";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.Control;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(158, 71);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(70, 13);
            this.textBox4.TabIndex = 41;
            this.textBox4.Text = "Data Size (UC)";
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.SystemColors.Control;
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox8.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox8.Enabled = false;
            this.textBox8.Location = new System.Drawing.Point(158, 19);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(70, 13);
            this.textBox8.TabIndex = 37;
            this.textBox8.Text = "Folder Count";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.Control;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox5.Enabled = false;
            this.textBox5.Location = new System.Drawing.Point(158, 45);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(70, 13);
            this.textBox5.TabIndex = 38;
            this.textBox5.Text = "File Count";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.txtFileIndex);
            this.groupBox1.Controls.Add(this.textBox9);
            this.groupBox1.Controls.Add(this.textBox13);
            this.groupBox1.Controls.Add(this.textBox17);
            this.groupBox1.Location = new System.Drawing.Point(168, 434);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(109, 121);
            this.groupBox1.TabIndex = 40;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "File Info";
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(42, 69);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(60, 20);
            this.textBox1.TabIndex = 46;
            this.textBox1.Text = "0";
            // 
            // textBox6
            // 
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox6.Enabled = false;
            this.textBox6.Location = new System.Drawing.Point(42, 43);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(60, 20);
            this.textBox6.TabIndex = 43;
            this.textBox6.Text = "0";
            // 
            // txtFileIndex
            // 
            this.txtFileIndex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFileIndex.Enabled = false;
            this.txtFileIndex.Location = new System.Drawing.Point(42, 17);
            this.txtFileIndex.Name = "txtFileIndex";
            this.txtFileIndex.Size = new System.Drawing.Size(60, 20);
            this.txtFileIndex.TabIndex = 42;
            this.txtFileIndex.Text = "0";
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.SystemColors.Control;
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox9.Enabled = false;
            this.textBox9.Location = new System.Drawing.Point(6, 71);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(30, 13);
            this.textBox9.TabIndex = 47;
            this.textBox9.Text = "Flags";
            // 
            // textBox13
            // 
            this.textBox13.BackColor = System.Drawing.SystemColors.Control;
            this.textBox13.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox13.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox13.Enabled = false;
            this.textBox13.Location = new System.Drawing.Point(6, 19);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(30, 13);
            this.textBox13.TabIndex = 44;
            this.textBox13.Text = "Index";
            // 
            // textBox17
            // 
            this.textBox17.BackColor = System.Drawing.SystemColors.Control;
            this.textBox17.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox17.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox17.Enabled = false;
            this.textBox17.Location = new System.Drawing.Point(6, 45);
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.Size = new System.Drawing.Size(30, 13);
            this.textBox17.TabIndex = 45;
            this.textBox17.Text = "Hash";
            // 
            // txtWorkingDirectory
            // 
            this.txtWorkingDirectory.BackColor = System.Drawing.SystemColors.Control;
            this.txtWorkingDirectory.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWorkingDirectory.Enabled = false;
            this.txtWorkingDirectory.Location = new System.Drawing.Point(192, 18);
            this.txtWorkingDirectory.Name = "txtWorkingDirectory";
            this.txtWorkingDirectory.Size = new System.Drawing.Size(302, 13);
            this.txtWorkingDirectory.TabIndex = 41;
            this.txtWorkingDirectory.Text = "No Working Directory...";
            // 
            // btnExport
            // 
            this.btnExport.Enabled = false;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.Location = new System.Drawing.Point(140, 15);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(46, 20);
            this.btnExport.TabIndex = 42;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // txtTableLength
            // 
            this.txtTableLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTableLength.Location = new System.Drawing.Point(12, 15);
            this.txtTableLength.MaxLength = 5;
            this.txtTableLength.Name = "txtTableLength";
            this.txtTableLength.Size = new System.Drawing.Size(70, 20);
            this.txtTableLength.TabIndex = 49;
            this.txtTableLength.Text = "64";
            // 
            // textBox16
            // 
            this.textBox16.BackColor = System.Drawing.SystemColors.Control;
            this.textBox16.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox16.Location = new System.Drawing.Point(363, 418);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(174, 13);
            this.textBox16.TabIndex = 50;
            this.textBox16.Text = "Team BONER :: 1894 - 4EVER LOL";
            this.textBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox16.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.textBox23);
            this.panel1.Controls.Add(this.textBox24);
            this.panel1.Controls.Add(this.textBox25);
            this.panel1.Controls.Add(this.textBox26);
            this.panel1.Controls.Add(this.textBox27);
            this.panel1.Controls.Add(this.textBox28);
            this.panel1.Controls.Add(this.textBox29);
            this.panel1.Controls.Add(this.textBox30);
            this.panel1.Controls.Add(this.textBox22);
            this.panel1.Controls.Add(this.textBox21);
            this.panel1.Controls.Add(this.textBox20);
            this.panel1.Controls.Add(this.textBox19);
            this.panel1.Controls.Add(this.textBox18);
            this.panel1.Controls.Add(this.textBox14);
            this.panel1.Controls.Add(this.textBox7);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Location = new System.Drawing.Point(500, 228);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(174, 154);
            this.panel1.TabIndex = 51;
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.SystemColors.Control;
            this.textBox23.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox23.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox23.Enabled = false;
            this.textBox23.Location = new System.Drawing.Point(89, 136);
            this.textBox23.Name = "textBox23";
            this.textBox23.ReadOnly = true;
            this.textBox23.Size = new System.Drawing.Size(80, 13);
            this.textBox23.TabIndex = 66;
            this.textBox23.Text = "0";
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.SystemColors.Control;
            this.textBox24.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox24.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox24.Enabled = false;
            this.textBox24.Location = new System.Drawing.Point(89, 117);
            this.textBox24.Name = "textBox24";
            this.textBox24.ReadOnly = true;
            this.textBox24.Size = new System.Drawing.Size(80, 13);
            this.textBox24.TabIndex = 65;
            this.textBox24.Text = "0";
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.SystemColors.Control;
            this.textBox25.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox25.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox25.Enabled = false;
            this.textBox25.Location = new System.Drawing.Point(89, 98);
            this.textBox25.Name = "textBox25";
            this.textBox25.ReadOnly = true;
            this.textBox25.Size = new System.Drawing.Size(80, 13);
            this.textBox25.TabIndex = 64;
            this.textBox25.Text = "0";
            // 
            // textBox26
            // 
            this.textBox26.BackColor = System.Drawing.SystemColors.Control;
            this.textBox26.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox26.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox26.Enabled = false;
            this.textBox26.Location = new System.Drawing.Point(89, 79);
            this.textBox26.Name = "textBox26";
            this.textBox26.ReadOnly = true;
            this.textBox26.Size = new System.Drawing.Size(80, 13);
            this.textBox26.TabIndex = 63;
            this.textBox26.Text = "0";
            // 
            // textBox27
            // 
            this.textBox27.BackColor = System.Drawing.SystemColors.Control;
            this.textBox27.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox27.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox27.Enabled = false;
            this.textBox27.Location = new System.Drawing.Point(89, 60);
            this.textBox27.Name = "textBox27";
            this.textBox27.ReadOnly = true;
            this.textBox27.Size = new System.Drawing.Size(80, 13);
            this.textBox27.TabIndex = 62;
            this.textBox27.Text = "0";
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.SystemColors.Control;
            this.textBox28.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox28.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox28.Enabled = false;
            this.textBox28.Location = new System.Drawing.Point(89, 41);
            this.textBox28.Name = "textBox28";
            this.textBox28.ReadOnly = true;
            this.textBox28.Size = new System.Drawing.Size(80, 13);
            this.textBox28.TabIndex = 61;
            this.textBox28.Text = "0";
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.SystemColors.Control;
            this.textBox29.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox29.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox29.Enabled = false;
            this.textBox29.Location = new System.Drawing.Point(89, 22);
            this.textBox29.Name = "textBox29";
            this.textBox29.ReadOnly = true;
            this.textBox29.Size = new System.Drawing.Size(80, 13);
            this.textBox29.TabIndex = 60;
            this.textBox29.Text = "0";
            // 
            // textBox30
            // 
            this.textBox30.BackColor = System.Drawing.SystemColors.Control;
            this.textBox30.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox30.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox30.Enabled = false;
            this.textBox30.Location = new System.Drawing.Point(89, 3);
            this.textBox30.Name = "textBox30";
            this.textBox30.ReadOnly = true;
            this.textBox30.Size = new System.Drawing.Size(80, 13);
            this.textBox30.TabIndex = 59;
            this.textBox30.Text = "0";
            // 
            // textBox22
            // 
            this.textBox22.BackColor = System.Drawing.SystemColors.Control;
            this.textBox22.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox22.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox22.Enabled = false;
            this.textBox22.Location = new System.Drawing.Point(3, 136);
            this.textBox22.Name = "textBox22";
            this.textBox22.ReadOnly = true;
            this.textBox22.Size = new System.Drawing.Size(80, 13);
            this.textBox22.TabIndex = 58;
            this.textBox22.Text = "File Count";
            // 
            // textBox21
            // 
            this.textBox21.BackColor = System.Drawing.SystemColors.Control;
            this.textBox21.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox21.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox21.Enabled = false;
            this.textBox21.Location = new System.Drawing.Point(3, 117);
            this.textBox21.Name = "textBox21";
            this.textBox21.ReadOnly = true;
            this.textBox21.Size = new System.Drawing.Size(80, 13);
            this.textBox21.TabIndex = 57;
            this.textBox21.Text = "Folder Count";
            // 
            // textBox20
            // 
            this.textBox20.BackColor = System.Drawing.SystemColors.Control;
            this.textBox20.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox20.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox20.Enabled = false;
            this.textBox20.Location = new System.Drawing.Point(3, 98);
            this.textBox20.Name = "textBox20";
            this.textBox20.ReadOnly = true;
            this.textBox20.Size = new System.Drawing.Size(80, 13);
            this.textBox20.TabIndex = 56;
            this.textBox20.Text = "Descriptor Count";
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.SystemColors.Control;
            this.textBox19.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox19.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox19.Enabled = false;
            this.textBox19.Location = new System.Drawing.Point(3, 79);
            this.textBox19.Name = "textBox19";
            this.textBox19.ReadOnly = true;
            this.textBox19.Size = new System.Drawing.Size(80, 13);
            this.textBox19.TabIndex = 55;
            this.textBox19.Text = "Table Size";
            // 
            // textBox18
            // 
            this.textBox18.BackColor = System.Drawing.SystemColors.Control;
            this.textBox18.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox18.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox18.Enabled = false;
            this.textBox18.Location = new System.Drawing.Point(3, 60);
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.Size = new System.Drawing.Size(80, 13);
            this.textBox18.TabIndex = 54;
            this.textBox18.Text = "Archive Size";
            // 
            // textBox14
            // 
            this.textBox14.BackColor = System.Drawing.SystemColors.Control;
            this.textBox14.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox14.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox14.Enabled = false;
            this.textBox14.Location = new System.Drawing.Point(3, 41);
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.Size = new System.Drawing.Size(80, 13);
            this.textBox14.TabIndex = 53;
            this.textBox14.Text = "Header Size";
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.SystemColors.Control;
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox7.Enabled = false;
            this.textBox7.Location = new System.Drawing.Point(3, 22);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(80, 13);
            this.textBox7.TabIndex = 52;
            this.textBox7.Text = "Version";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Control;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(3, 3);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(80, 13);
            this.textBox2.TabIndex = 51;
            this.textBox2.Text = "Indentifier";
            // 
            // cbxInfoPanelSelection
            // 
            this.cbxInfoPanelSelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxInfoPanelSelection.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbxInfoPanelSelection.FormattingEnabled = true;
            this.cbxInfoPanelSelection.Items.AddRange(new object[] {
            "Archive Header",
            "Entry Descriptor",
            "Extra File Info"});
            this.cbxInfoPanelSelection.Location = new System.Drawing.Point(320, 41);
            this.cbxInfoPanelSelection.Name = "cbxInfoPanelSelection";
            this.cbxInfoPanelSelection.Size = new System.Drawing.Size(174, 21);
            this.cbxInfoPanelSelection.TabIndex = 54;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.textBox31);
            this.panel2.Controls.Add(this.textBox32);
            this.panel2.Controls.Add(this.textBox33);
            this.panel2.Controls.Add(this.textBox34);
            this.panel2.Controls.Add(this.textBox35);
            this.panel2.Controls.Add(this.textBox36);
            this.panel2.Controls.Add(this.textBox37);
            this.panel2.Controls.Add(this.textBox38);
            this.panel2.Controls.Add(this.textBox39);
            this.panel2.Controls.Add(this.textBox40);
            this.panel2.Controls.Add(this.textBox41);
            this.panel2.Controls.Add(this.textBox42);
            this.panel2.Controls.Add(this.textBox43);
            this.panel2.Controls.Add(this.textBox44);
            this.panel2.Controls.Add(this.textBox45);
            this.panel2.Controls.Add(this.textBox46);
            this.panel2.Location = new System.Drawing.Point(500, 68);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(174, 154);
            this.panel2.TabIndex = 67;
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.SystemColors.Control;
            this.textBox31.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox31.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox31.Enabled = false;
            this.textBox31.Location = new System.Drawing.Point(89, 136);
            this.textBox31.Name = "textBox31";
            this.textBox31.ReadOnly = true;
            this.textBox31.Size = new System.Drawing.Size(80, 13);
            this.textBox31.TabIndex = 66;
            this.textBox31.Text = "0";
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.SystemColors.Control;
            this.textBox32.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox32.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox32.Enabled = false;
            this.textBox32.Location = new System.Drawing.Point(89, 117);
            this.textBox32.Name = "textBox32";
            this.textBox32.ReadOnly = true;
            this.textBox32.Size = new System.Drawing.Size(80, 13);
            this.textBox32.TabIndex = 65;
            this.textBox32.Text = "0";
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.SystemColors.Control;
            this.textBox33.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox33.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox33.Enabled = false;
            this.textBox33.Location = new System.Drawing.Point(89, 98);
            this.textBox33.Name = "textBox33";
            this.textBox33.ReadOnly = true;
            this.textBox33.Size = new System.Drawing.Size(80, 13);
            this.textBox33.TabIndex = 64;
            this.textBox33.Text = "0";
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.SystemColors.Control;
            this.textBox34.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox34.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox34.Enabled = false;
            this.textBox34.Location = new System.Drawing.Point(89, 79);
            this.textBox34.Name = "textBox34";
            this.textBox34.ReadOnly = true;
            this.textBox34.Size = new System.Drawing.Size(80, 13);
            this.textBox34.TabIndex = 63;
            this.textBox34.Text = "0";
            // 
            // textBox35
            // 
            this.textBox35.BackColor = System.Drawing.SystemColors.Control;
            this.textBox35.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox35.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox35.Enabled = false;
            this.textBox35.Location = new System.Drawing.Point(89, 60);
            this.textBox35.Name = "textBox35";
            this.textBox35.ReadOnly = true;
            this.textBox35.Size = new System.Drawing.Size(80, 13);
            this.textBox35.TabIndex = 62;
            this.textBox35.Text = "0";
            // 
            // textBox36
            // 
            this.textBox36.BackColor = System.Drawing.SystemColors.Control;
            this.textBox36.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox36.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox36.Enabled = false;
            this.textBox36.Location = new System.Drawing.Point(89, 41);
            this.textBox36.Name = "textBox36";
            this.textBox36.ReadOnly = true;
            this.textBox36.Size = new System.Drawing.Size(80, 13);
            this.textBox36.TabIndex = 61;
            this.textBox36.Text = "0";
            // 
            // textBox37
            // 
            this.textBox37.BackColor = System.Drawing.SystemColors.Control;
            this.textBox37.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox37.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox37.Enabled = false;
            this.textBox37.Location = new System.Drawing.Point(89, 22);
            this.textBox37.Name = "textBox37";
            this.textBox37.ReadOnly = true;
            this.textBox37.Size = new System.Drawing.Size(80, 13);
            this.textBox37.TabIndex = 60;
            this.textBox37.Text = "0";
            // 
            // textBox38
            // 
            this.textBox38.BackColor = System.Drawing.SystemColors.Control;
            this.textBox38.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox38.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox38.Enabled = false;
            this.textBox38.Location = new System.Drawing.Point(89, 3);
            this.textBox38.Name = "textBox38";
            this.textBox38.ReadOnly = true;
            this.textBox38.Size = new System.Drawing.Size(80, 13);
            this.textBox38.TabIndex = 59;
            this.textBox38.Text = "0";
            // 
            // textBox39
            // 
            this.textBox39.BackColor = System.Drawing.SystemColors.Control;
            this.textBox39.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox39.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox39.Enabled = false;
            this.textBox39.Location = new System.Drawing.Point(3, 136);
            this.textBox39.Name = "textBox39";
            this.textBox39.ReadOnly = true;
            this.textBox39.Size = new System.Drawing.Size(80, 13);
            this.textBox39.TabIndex = 58;
            this.textBox39.Text = "File Count";
            // 
            // textBox40
            // 
            this.textBox40.BackColor = System.Drawing.SystemColors.Control;
            this.textBox40.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox40.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox40.Enabled = false;
            this.textBox40.Location = new System.Drawing.Point(3, 117);
            this.textBox40.Name = "textBox40";
            this.textBox40.ReadOnly = true;
            this.textBox40.Size = new System.Drawing.Size(80, 13);
            this.textBox40.TabIndex = 57;
            this.textBox40.Text = "Folder Count";
            // 
            // textBox41
            // 
            this.textBox41.BackColor = System.Drawing.SystemColors.Control;
            this.textBox41.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox41.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox41.Enabled = false;
            this.textBox41.Location = new System.Drawing.Point(3, 98);
            this.textBox41.Name = "textBox41";
            this.textBox41.ReadOnly = true;
            this.textBox41.Size = new System.Drawing.Size(80, 13);
            this.textBox41.TabIndex = 56;
            this.textBox41.Text = "Descriptor Count";
            // 
            // textBox42
            // 
            this.textBox42.BackColor = System.Drawing.SystemColors.Control;
            this.textBox42.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox42.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox42.Enabled = false;
            this.textBox42.Location = new System.Drawing.Point(3, 79);
            this.textBox42.Name = "textBox42";
            this.textBox42.ReadOnly = true;
            this.textBox42.Size = new System.Drawing.Size(80, 13);
            this.textBox42.TabIndex = 55;
            this.textBox42.Text = "Table Size";
            // 
            // textBox43
            // 
            this.textBox43.BackColor = System.Drawing.SystemColors.Control;
            this.textBox43.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox43.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox43.Enabled = false;
            this.textBox43.Location = new System.Drawing.Point(3, 60);
            this.textBox43.Name = "textBox43";
            this.textBox43.ReadOnly = true;
            this.textBox43.Size = new System.Drawing.Size(80, 13);
            this.textBox43.TabIndex = 54;
            this.textBox43.Text = "Archive Size";
            // 
            // textBox44
            // 
            this.textBox44.BackColor = System.Drawing.SystemColors.Control;
            this.textBox44.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox44.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox44.Enabled = false;
            this.textBox44.Location = new System.Drawing.Point(3, 41);
            this.textBox44.Name = "textBox44";
            this.textBox44.ReadOnly = true;
            this.textBox44.Size = new System.Drawing.Size(80, 13);
            this.textBox44.TabIndex = 53;
            this.textBox44.Text = "Header Size";
            // 
            // textBox45
            // 
            this.textBox45.BackColor = System.Drawing.SystemColors.Control;
            this.textBox45.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox45.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox45.Enabled = false;
            this.textBox45.Location = new System.Drawing.Point(3, 22);
            this.textBox45.Name = "textBox45";
            this.textBox45.ReadOnly = true;
            this.textBox45.Size = new System.Drawing.Size(80, 13);
            this.textBox45.TabIndex = 52;
            this.textBox45.Text = "Version";
            // 
            // textBox46
            // 
            this.textBox46.BackColor = System.Drawing.SystemColors.Control;
            this.textBox46.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox46.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox46.Enabled = false;
            this.textBox46.Location = new System.Drawing.Point(3, 3);
            this.textBox46.Name = "textBox46";
            this.textBox46.ReadOnly = true;
            this.textBox46.Size = new System.Drawing.Size(80, 13);
            this.textBox46.TabIndex = 51;
            this.textBox46.Text = "Indentifier";
            // 
            // textBox47
            // 
            this.textBox47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox47.Enabled = false;
            this.textBox47.Location = new System.Drawing.Point(320, 68);
            this.textBox47.Multiline = true;
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(174, 173);
            this.textBox47.TabIndex = 68;
            // 
            // picTexture
            // 
            this.picTexture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picTexture.Location = new System.Drawing.Point(691, 343);
            this.picTexture.Name = "picTexture";
            this.picTexture.Size = new System.Drawing.Size(128, 128);
            this.picTexture.TabIndex = 38;
            this.picTexture.TabStop = false;
            this.picTexture.Visible = false;
            // 
            // frmArchive
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 627);
            this.Controls.Add(this.textBox47);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.cbxInfoPanelSelection);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBox16);
            this.Controls.Add(this.txtTableLength);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.txtWorkingDirectory);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.picTexture);
            this.Controls.Add(this.grpArchiveInfo);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.trvDirectory);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(455, 419);
            this.Name = "frmArchive";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Archive Manager";
            this.Load += new System.EventHandler(this.frmArchive_Load);
            this.grpArchiveInfo.ResumeLayout(false);
            this.grpArchiveInfo.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTexture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView trvDirectory;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.GroupBox grpArchiveInfo;
        private System.Windows.Forms.TextBox txtFileCount;
        private System.Windows.Forms.TextBox txtFolderCount;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtWorkingDirectory;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox txtDataSizeC;
        private System.Windows.Forms.TextBox txtDataSizeUC;
        private System.Windows.Forms.TextBox txtTableLength;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox txtTableSize;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox txtRecordSize;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox txtDescriptorSize;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox txtTableEntries;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox txtFileIndex;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cbxInfoPanelSelection;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.PictureBox picTexture;
    }
}


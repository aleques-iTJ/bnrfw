﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bnrToolkit
{
    public partial class frmTexture : Form
    {
        bnrToolkit.Texture texture;

        public frmTexture()
        {
            InitializeComponent();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog files    = new OpenFileDialog();
            files.InitialDirectory  = "C:\\";
            files.Filter            = "Image Files(*.bmp; *.png; *.dds)|*.bmp; *.png; *.dds";
            files.RestoreDirectory  = true;

            if (files.ShowDialog() == DialogResult.OK)
            {
                texture = Texture.FromFile(files.FileName);

                if (texture != null)
                {
                    txtTextureInfo.Text = texture.ToString();
                    picTexture.Image    = texture.GetImage();
                }

                btnExport.Enabled = true;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog files    = new SaveFileDialog();

            files.InitialDirectory  = "C:\\";
            files.Filter            = "bnrFW Texture Container File(*.bnr)|*.bnr";
            files.RestoreDirectory  = true;

            if (files.ShowDialog() == DialogResult.OK)
                texture.ToFile(files.FileName, chkCompress.Checked);  
        }


        private void chkCompress_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCompress.Checked == true)
                texture.Flags &= (ushort)Texture.Descriptor.IsCompressed;

            else
                texture.Flags &= (ushort)~Texture.Descriptor.IsCompressed;
        }

        private void chkAnimated_CheckedChanged(object sender, EventArgs e)
        {
            /*if (chkCompress.Checked == true)
                texture.Flags &= (ushort)Texture.Descriptor.IsAnimated;

            else
                texture.Flags &= (ushort)~Texture.Descriptor.IsAnimated;*/
        }

        private void cbxFilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cbxFilterType.SelectedIndex)
            {
                case 0:
                    texture.Flags &= (ushort)Texture.Descriptor.Nearest;
                    break;

                case 1:
                    texture.Flags &= (ushort)Texture.Descriptor.Trilinear;
                    break;

                case 2:
                    texture.Flags &= (ushort)Texture.Descriptor.Anisotropic;
                    break;


                default:
                    texture.Flags &= (ushort)Texture.Descriptor.Nearest;
                    break;
            }
        }

        private void cbxFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cbxFormat.SelectedIndex)
            {
                case 0:
                    texture.Flags &= (ushort)Texture.Descriptor.RGBA;
                    break;

                case 1:
                    texture.Flags &= (ushort)Texture.Descriptor.DXT1;
                    break;

                case 2:
                    texture.Flags &= (ushort)Texture.Descriptor.DXT5;
                    break;


                default:
                    texture.Flags &= (ushort)Texture.Descriptor.RGBA;
                    break;
            }
        }


        private void chkGenMips_CheckedChanged(object sender, EventArgs e)
        {
            txtMipNum.Enabled = chkGenMips.Checked;

            if (chkGenMips.Checked == true)
                texture.GenerateMipMaps(GetMipMode());

            else
                texture.DestroyMipMaps();

            picTexture.Image    = texture.GetImage();
            txtTextureInfo.Text = texture.ToString();
        }

        private void cbxMipmapResampleType_SelectedIndexChanged(object sender, EventArgs e)
        {
            texture.GenerateMipMaps(GetMipMode());

            picTexture.Image    = texture.GetImage();
            txtTextureInfo.Text = texture.ToString();
        }

        private void txtMipNum_TextChanged(object sender, EventArgs e)
        {
            int  num;
            bool valid = int.TryParse(txtMipNum.Text, out num);

            if (valid)
            {
                if (num >= 0 && num <= texture.MipMapCount)
                {
                    if (num == 0)
                        picTexture.Image = texture.GetImage();

                    else
                    {
                        // The array is zero indexed, but they're counted normally
                        num -= 1;
                        picTexture.Image = texture.FromMip(num);
                    }
                }
            }
        }


        // Helper
        private System.Drawing.Drawing2D.InterpolationMode GetMipMode()
        {
            System.Drawing.Drawing2D.InterpolationMode mode;

            switch (cbxMipmapResampleType.SelectedIndex)
            {
                case 0: // Nearest
                    mode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                    break;

                case 1: // Bilinear
                    mode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
                    break;

                case 2: // Bicubic
                    mode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    break;

                default:
                    mode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                    break;
            }

            return mode;
        }

        // cactus
    }
}
﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

using System.IO;
using System.Collections.Generic;

namespace bnrToolkit
{
    // TODO: CLEAN UP

    public partial class frmArchive : Form
    {
        //Archive.F archive;
        int archiveDataOffset;

        public frmArchive()
        {
            InitializeComponent();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog files = new OpenFileDialog();
            files.InitialDirectory = "C:\\";
            files.Filter = "Image Files(*.bmp; *.png; *.jpg; *.gif)|*.bmp; *.png; *.jpg; *.gif";
            files.RestoreDirectory = true;

            if (files.ShowDialog() == DialogResult.OK)
            {
                picTexture.Image = Image.FromFile(files.FileName);
                picTexture.Width = picTexture.Image.Width;
                picTexture.Height = picTexture.Image.Height;

                if (picTexture.Width > this.Width)
                {
                    // Resize it and fudge it a bit to compensate for the padding
                    // TODO: What was I honestly doing here?
                    this.Width = picTexture.Width + 40;
                }

                ToolTip picTooltip = new ToolTip();
                picTooltip.SetToolTip(this.picTexture, "Image data!\n" +
                                                        "----------------" +
                                                        "\nWidth:\t\t" + picTexture.Width.ToString() +
                                                        "\nHeight:\t\t" + picTexture.Height.ToString() +
                                                        "\nPixel Format:\t" + picTexture.Image.PixelFormat.ToString());
            }

            // ???
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*  Bitmap bmp = new Bitmap(picTexture.Image);
              Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
              BitmapData data = bmp.LockBits(rect, ImageLockMode.ReadWrite, bmp.PixelFormat);
              IntPtr ptr = data.Scan0; // Pointer to the first pixel in the bitmap      

              int dataSize = data.Stride * bmp.Height;
              Texture texture = new Texture();
              //texture.Header.Identifier = "btx";
              texture.Header.OldLength = texture.Header.Length + (data.Stride * bmp.Height);
              texture.Header.Width = bmp.Width;
              texture.Header.Height = bmp.Height;

              byte[] pixels = new byte[dataSize];
              System.Runtime.InteropServices.Marshal.Copy(ptr, pixels, 0, dataSize);

              //if (chkHighComp.Checked == true)
              {
                  texture.Data = LZ4.LZ4Codec.EncodeHC(pixels, 0, dataSize);
                  texture.Header.NewLength = texture.Data.Length;

                  // Kind of roundabout in C#... but StructureToPtr says enough
                  byte[] buffer = new byte[texture.Length];
                  var gch = System.Runtime.InteropServices.GCHandle.Alloc(buffer, System.Runtime.InteropServices.GCHandleType.Pinned);
                  System.Runtime.InteropServices.Marshal.StructureToPtr(texture, gch.AddrOfPinnedObject(), true);

                  // Done, write out the file
                  //BinaryWriter bw = new BinaryWriter(File.Open(txtFilePath.Text + "_btx", FileMode.OpenOrCreate));
                  // bw.Write(buffer, 0, buffer.Length);
                  // bw.Flush();
                  // bw.Close();

                  gch.Free();
              }

              // else
              {

                  //compressed = LZ4.LZ4Codec.Encode(pixels, 0, size);
              }*/
        }

        /*private void BuildTree(DirectoryInfo directoryInfo, TreeNodeCollection addInMe)
        {
            TreeNode curNode;

            foreach (DirectoryInfo subdir in directoryInfo.GetDirectories())
            {
                curNode = addInMe.Add(subdir.Name);

                foreach (FileInfo file in subdir.GetFiles())
                {
                    curNode.Nodes.Add(file.FullName, file.Name);
                }
            }
        }*/

        private Archive.EntryDescriptor folderDesc = new Archive.EntryDescriptor();
        private Archive.EntryDescriptor fileDesc = new Archive.EntryDescriptor();

        private void CreateDirTree(TreeNodeCollection nodes, string path)
        {
            var root = new DirectoryInfo(path);

            foreach (var dir in root.GetDirectories())
            {
                // Up the global folder count and mark this path as being a folder
                //archive.Header.FolderCount += 1;
                folderDesc.Flags = Archive.PathFlags.IsFolder;

                // Actually add the node
                TreeNode dirNode = new TreeNode(dir.Name);
                nodes.Add(dirNode);

                CreateDirTree(dirNode.Nodes, dir.FullName);
            }

            foreach (var file in root.GetFiles())
            {
                // Up the global file count
                //archive.Header.FileCount += 1;

                // Actually add the node
                TreeNode t = new TreeNode(file.Name);
                nodes.Add(t);

                // Set up the file name
                string actualFilePath = txtWorkingDirectory.Text + t.FullPath;

                // Actually read the thing, and read everything uncompressed to start
                byte[] data = File.ReadAllBytes(actualFilePath);
                //fd.UncompressedLength   = data.Length;

                fileDesc.UncompressedSize = data.Length;

                // Write it off to the data section, if there is anything
                if (data.Length > 0)
                {
                    //archive.DataSection.Write(data, 0, data.Length);
                    archiveDataOffset += data.Length;

                    uint size = Convert.ToUInt32(txtTableLength.Text);
                    //uint index = Archive.Hasher.Hash(t.FullPath) & (size - 1); // 0 indexed, ie 0-63
                    //archive.ContentTable.
                    //archive.TableEntries.FilePathHashes.Add(ArchiveHash.Hash(t.FullPath));
                    //archive.TableEntries.FileDescriptors.Insert((int)index, fd);


                    /*if ((size & (size - 1)) == 0)
                    {
                        //uint index = ArchiveHash.Hash(t.FullPath) & (size - 1); // 0 indexed, ie 0-63
                        //archive.TableEntries.FilePathHashes.Add(ArchiveHash.Hash(t.FullPath));
                        //archive.TableEntries.FileDescriptors.Insert((int)index, fd);
                    }

                    else
                    {
                        txtWorkingDirectory.Text = "Table malformed, use power of 2 for length.";
                    }*/
                }
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            // Set up whatever we can for the archive
            //archive = new Archive.ArchiveFile();

            // Gross... grow the table size appropriately
            int size = Convert.ToInt32(txtTableLength.Text);
            for (int i = 0; i < size; i++)
            //    archive.ContentTable.Add(new Archive.EntryDescriptor());

            // Obliterate the tree view if something is there already and get reader to load something
            trvDirectory.Nodes.Clear();
            FolderBrowserDialog files = new FolderBrowserDialog();

            if (files.ShowDialog() == DialogResult.OK)
            {
                //Is it ever possible to OK an invalid directory?
                txtWorkingDirectory.Text = files.SelectedPath + "\\";
                CreateDirTree(trvDirectory.Nodes, files.SelectedPath);

                // Finish setting up the data section
               // byte[] dataUncompressed = archive.DataSection.ToArray();
                //byte[] dataCompressed = LZ4.LZ4Codec.EncodeHC(dataUncompressed, 0, dataUncompressed.Length);

                // Aaaand pass along some info
               // txtFolderCount.Text = archive.Header.FolderCount.ToString();
               // txtFileCount.Text = archive.Header.FileCount.ToString();

                // Yuck
                //txtTableEntries.Text    = archive.TableEntries.FilePathHashes.Count.ToString();
                //txtTableSize.Text       = (archive.TableEntries.FilePathHashes.Count * sizeof(uint)).ToString();
                //txtDescriptorSize.Text  = (archive.TableEntries.FileDescriptors.Count * archive.Header.DescriptorSize).ToString();

                int total = Convert.ToInt32(txtTableSize.Text) + Convert.ToInt32(txtDescriptorSize.Text);
                txtRecordSize.Text = total.ToString();

               // txtDataSizeUC.Text  = dataUncompressed.Length.ToString();
               // txtDataSizeC.Text   = dataCompressed.Length.ToString();

                btnExport.Enabled = true;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmAbout about = new frmAbout();
            about.Show();
        }

        private void frmArchive_Load(object sender, EventArgs e)
        {
            cbxInfoPanelSelection.SelectedIndex = 0;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {

        }
    }
}
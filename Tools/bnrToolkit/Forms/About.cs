﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bnrToolkit
{
    public partial class frmAbout : Form
    {
        public frmAbout()
        {
            InitializeComponent();
        }

        private void About_Load(object sender, EventArgs e)
        {
            txtAbout.AppendText(FarewellStrings.GetRandomString());

            txtAbout.SelectAll();
            txtAbout.SelectionAlignment = HorizontalAlignment.Center;            
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMoreInfo_Click(object sender, EventArgs e)
        {
            btnMoreInfo.Text = "Yeah, go on...";
        }
    }
}

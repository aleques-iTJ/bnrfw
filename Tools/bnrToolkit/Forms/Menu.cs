﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Drawing.Imaging;

namespace bnrToolkit.Forms
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        // Form should be dragable, even without a border
        private const int WM_NCHITTEST  = 0x84;
        private const int HTCLIENT      = 0x01;
        private const int HTCAPTION     = 0x02;

        protected override void WndProc(ref Message message)
        {
            base.WndProc(ref message);

            if (message.Msg == WM_NCHITTEST && (int)message.Result == HTCLIENT)
                message.Result = (IntPtr)HTCAPTION;
        }


        // Grapes
        private bool areControlsVisible = true;
        private DIBSection dibSection;
        private Starfield starfield;

        private void Menu_Load(object sender, EventArgs e)
        {
            dibSection  = new DIBSection(this.Handle, this.Width, this.Height);
            starfield   = new Starfield(this.Width, this.Height, 256);
        }

        // Ghetto, but just use a timer to tick the renderer
        private void gfxTimer_Tick(object sender, EventArgs e)
        {
            starfield.UpdateAndDraw(ref dibSection);
        }


        private void btnOpenTexForm_Click(object sender, EventArgs e)
        {
            if ((Application.OpenForms["frmTexture"] as frmTexture) == null)
            {
                frmTexture form = new frmTexture();
                form.Show();
            }
        }

        private void btnOpenArchiveForm_Click(object sender, EventArgs e)
        {
            if ((Application.OpenForms["frmArchive"] as frmArchive) == null)
            {
                frmArchive form = new frmArchive();
                form.Show();
            }
        }

        private void btnOpenAboutForm_Click(object sender, EventArgs e)
        {
            if ((Application.OpenForms["frmAbout"] as frmAbout) == null)
            {
                frmAbout form = new frmAbout();
                form.Show();
            }
        }


        private void btnHide_Click(object sender, EventArgs e)
        {
            if (areControlsVisible == true)
                btnHide.Text = "Show";
            else
                btnHide.Text = "Hide";

            // Just flip the condition
            areControlsVisible = !areControlsVisible;

            foreach (Control control in this.Controls)
                control.Visible = areControlsVisible;

            // This should never be hidden
            btnHide.Visible = true;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
﻿namespace bnrToolkit.Forms
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnOpenTexForm = new System.Windows.Forms.Button();
            this.btnOpenArchiveForm = new System.Windows.Forms.Button();
            this.btnOpenMeshForm = new System.Windows.Forms.Button();
            this.gfxTimer = new System.Windows.Forms.Timer(this.components);
            this.btnOpenAboutForm = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOpenTexForm
            // 
            this.btnOpenTexForm.BackColor = System.Drawing.Color.Black;
            this.btnOpenTexForm.FlatAppearance.BorderColor = System.Drawing.SystemColors.WindowFrame;
            this.btnOpenTexForm.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnOpenTexForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpenTexForm.ForeColor = System.Drawing.Color.White;
            this.btnOpenTexForm.Location = new System.Drawing.Point(12, 12);
            this.btnOpenTexForm.Name = "btnOpenTexForm";
            this.btnOpenTexForm.Size = new System.Drawing.Size(60, 25);
            this.btnOpenTexForm.TabIndex = 0;
            this.btnOpenTexForm.TabStop = false;
            this.btnOpenTexForm.Text = "Texture";
            this.btnOpenTexForm.UseVisualStyleBackColor = false;
            this.btnOpenTexForm.Click += new System.EventHandler(this.btnOpenTexForm_Click);
            // 
            // btnOpenArchiveForm
            // 
            this.btnOpenArchiveForm.BackColor = System.Drawing.Color.Black;
            this.btnOpenArchiveForm.FlatAppearance.BorderColor = System.Drawing.SystemColors.WindowFrame;
            this.btnOpenArchiveForm.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnOpenArchiveForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpenArchiveForm.ForeColor = System.Drawing.Color.White;
            this.btnOpenArchiveForm.Location = new System.Drawing.Point(12, 105);
            this.btnOpenArchiveForm.Name = "btnOpenArchiveForm";
            this.btnOpenArchiveForm.Size = new System.Drawing.Size(60, 25);
            this.btnOpenArchiveForm.TabIndex = 1;
            this.btnOpenArchiveForm.TabStop = false;
            this.btnOpenArchiveForm.Text = "Archive";
            this.btnOpenArchiveForm.UseVisualStyleBackColor = false;
            this.btnOpenArchiveForm.Click += new System.EventHandler(this.btnOpenArchiveForm_Click);
            // 
            // btnOpenMeshForm
            // 
            this.btnOpenMeshForm.BackColor = System.Drawing.Color.Black;
            this.btnOpenMeshForm.FlatAppearance.BorderColor = System.Drawing.SystemColors.WindowFrame;
            this.btnOpenMeshForm.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnOpenMeshForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpenMeshForm.ForeColor = System.Drawing.Color.White;
            this.btnOpenMeshForm.Location = new System.Drawing.Point(12, 43);
            this.btnOpenMeshForm.Name = "btnOpenMeshForm";
            this.btnOpenMeshForm.Size = new System.Drawing.Size(60, 25);
            this.btnOpenMeshForm.TabIndex = 2;
            this.btnOpenMeshForm.TabStop = false;
            this.btnOpenMeshForm.Text = "Mesh";
            this.btnOpenMeshForm.UseVisualStyleBackColor = false;
            // 
            // gfxTimer
            // 
            this.gfxTimer.Enabled = true;
            this.gfxTimer.Interval = 10;
            this.gfxTimer.Tick += new System.EventHandler(this.gfxTimer_Tick);
            // 
            // btnOpenAboutForm
            // 
            this.btnOpenAboutForm.BackColor = System.Drawing.Color.Black;
            this.btnOpenAboutForm.FlatAppearance.BorderColor = System.Drawing.SystemColors.WindowFrame;
            this.btnOpenAboutForm.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnOpenAboutForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpenAboutForm.ForeColor = System.Drawing.Color.White;
            this.btnOpenAboutForm.Location = new System.Drawing.Point(204, 142);
            this.btnOpenAboutForm.Name = "btnOpenAboutForm";
            this.btnOpenAboutForm.Size = new System.Drawing.Size(48, 24);
            this.btnOpenAboutForm.TabIndex = 45;
            this.btnOpenAboutForm.TabStop = false;
            this.btnOpenAboutForm.Text = "About";
            this.btnOpenAboutForm.UseVisualStyleBackColor = false;
            this.btnOpenAboutForm.Click += new System.EventHandler(this.btnOpenAboutForm_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.Color.Black;
            this.btnHide.FlatAppearance.BorderColor = System.Drawing.SystemColors.WindowFrame;
            this.btnHide.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnHide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHide.ForeColor = System.Drawing.Color.White;
            this.btnHide.Location = new System.Drawing.Point(258, 142);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(48, 24);
            this.btnHide.TabIndex = 46;
            this.btnHide.TabStop = false;
            this.btnHide.Text = "Hide";
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.SystemColors.WindowFrame;
            this.btnExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(150, 142);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(48, 24);
            this.btnExit.TabIndex = 47;
            this.btnExit.TabStop = false;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.FlatAppearance.BorderColor = System.Drawing.SystemColors.WindowFrame;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(12, 74);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(60, 25);
            this.button1.TabIndex = 48;
            this.button1.TabStop = false;
            this.button1.Text = "Font";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 178);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnHide);
            this.Controls.Add(this.btnOpenAboutForm);
            this.Controls.Add(this.btnOpenMeshForm);
            this.Controls.Add(this.btnOpenArchiveForm);
            this.Controls.Add(this.btnOpenTexForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmMenu";
            this.Load += new System.EventHandler(this.Menu_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOpenTexForm;
        private System.Windows.Forms.Button btnOpenArchiveForm;
        private System.Windows.Forms.Button btnOpenMeshForm;
        private System.Windows.Forms.Timer gfxTimer;
        private System.Windows.Forms.Button btnOpenAboutForm;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button button1;
    }
}
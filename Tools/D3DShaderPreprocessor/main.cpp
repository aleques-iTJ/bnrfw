#pragma once
#include "bnr.h"

// The renderer is semi-broken right now, expect a blank screen if it builds

#if defined (_DEBUG)
#pragma comment(lib, "Core-Debug-x64.lib")
#pragma comment(lib, "Video-Debug-x64.lib")
#endif
//----------------------------------------------------------------

class TestApp
{
public:
	TestApp();
	~TestApp();

	void Run();

private:
	Window*		window;
	Renderer*	renderer;
};

TestApp::TestApp()
{
	g_FileSystem = new FileSystem;

	window		= new Window;
	renderer	= new Renderer;

	String basePath = System::GetExeDirectory();
}

TestApp::~TestApp()
{
	// Reverse order from init
	if (renderer)	delete renderer;
	if (window)		delete window;
}

//----------------------------------------------------------------

void TestApp::Run()
{
	const u32 width		= 16;
	const u32 height	= 16;

	window->Create("bnr", width, height, Window::Style::Normal);	// Typical window
	renderer->Create(nullptr, width, height);
}

int main()
{
	TestApp app;
	app.Run();

	return 0;
}
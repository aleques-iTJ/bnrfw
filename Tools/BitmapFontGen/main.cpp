#pragma once
#include <Windows.h>
#include <cstdlib>
#include <cstdio>

#include "Core\FileSystem\File.h"
#include "Math\Rect.h"


// DEPRECIATED, OLD, AND SEMI BROKEN

// bnrFontGen
// Parses a bitmap image and craps out character information
// Size in pixels, texture coordinates, etc.


int main()
{
	File*   bitmap		= FileSystem::OpenFile("C:\\font.bmp", FileAccess::Read);
	u32		bitmapSize	= FileSystem::ReadFile(bitmap);
	u8*		data		= static_cast<u8*>(bitmap->GetDataPtr());

	struct BitmapHeader
	{
		u32     Width;
		u32     Height;
		u16     Depth;
		u32     RawSize;
		u32*	Data;
	}header;

	memset(&header, 0, sizeof(BitmapHeader));
	header.Width	= *(u32*)&data[0x12]; // 18
	header.Height	= *(u32*)&data[0x16]; // 22
	header.Depth	= *(u16*)&data[0x1C];
	header.RawSize	= *(u32*)&data[0x22];

	u32 offset		= data[0x0A];
	header.Data		= (u32*)((u8*)data + offset);

	/*

		http://en.wikipedia.org/wiki/BMP_file_format

		0Eh     4       the size of this header (40 bytes)
		12h     4       the bitmap width in pixels (signed integer).
		16h     4       the bitmap height in pixels (signed integer).
		1Ah     2       the number of color planes being used. Must be set to 1.
		1Ch     2       the number of bits per pixel, which is the color depth of the image. Typical values are 1, 4, 8, 16, 24 and 32.
		1Eh     4       the compression method being used. See the next table for a list of possible values.
		22h     4       the image size. This is the size of the raw bitmap data (see below), and should not be confused with the file size.
		26h     4       the horizontal resolution of the image. (pixel per meter, signed integer)
		2Ah     4       the vertical resolution of the image. (pixel per meter, signed integer)
		2Eh     4       the number of colors in the color palette, or 0 to default to 2n.
		32h     4       the number of important colors used, or 0 when every color is important; generally ignored.

	*/

	s32 dong = header.Height;
	BITMAPINFO      bi		= { 0 };
	bi.bmiHeader.biSize		= sizeof(BITMAPINFO);
	bi.bmiHeader.biWidth	= header.Width;
	bi.bmiHeader.biHeight	= header.Height;        // DIB Section is top down -- 0x, 0y is the top left
	bi.bmiHeader.biPlanes	= 1;
	bi.bmiHeader.biBitCount = 32;

	// Surface is just a simple array of 32 bit pixels
	u32* texture = new u32[(header.Width * header.Height) * 4];

	// And just set GDI up, we're using a DIB section as it provides direct access to the texture data
	// and it's much, much quicker to draw with than the native SetPixel() garbage
	HWND    hwnd	= GetForegroundWindow();
	HDC		hdc		= GetDC(hwnd);
	HDC		hdcMem	= CreateCompatibleDC(hdc);
	HBITMAP dib		= CreateDIBSection(hdc, &bi, DIB_RGB_COLORS, (void**)&texture, 0, 0);
	HBITMAP hbmOld	= (HBITMAP)SelectObject(hdcMem, dib);

	struct Character
	{
		Rect<f32>       UVCoord;
		Rect<u32>       PixelCoord;
		u8				Char;
	};

	cstr            inputString = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_";
	u32				size		= sizeof(Character) * strlen(inputString);
	Character*      characters	= (Character*)malloc(size);
	memset(characters, 0, size);

	s32 i		= 0;
	u32 startX	= 0;
	u32 oldX	= 0;
	u32 topGap	= 0;
	u32 botGap	= 0;
	bool empty	= true;

	for (u32 x = 0; x < header.Width; x++)
	{
		for (u32 y = 0; y < header.Height; y++)
		{
			// Draw to a texture as well for debugging
			u32 index		= y * header.Width + x;
			texture[index]	= header.Data[index] >> 8;

			characters[i].Char = inputString[i];

			// Hit a non-transparent / ignorable pixel
			if (texture[index] != 0x00FF00FF)
			{
				// Which means there's obviously something in this column
				empty = false;

				// Update the height if it's greater
				if ((header.Height - y) > characters[i].PixelCoord.Height)
				{
					characters[i].PixelCoord.Height	= (header.Height - y);
					characters[i].UVCoord.Y			= (f32)y / (f32)header.Height;
					characters[i].UVCoord.Height	= (f32)characters[i].PixelCoord.Height / (f32)header.Height;
				}
			}

			// Skippable pixel
			else if (characters[i].PixelCoord.Height != header.Height)
			{
				// We've previously hit something, there's a gap, and need to deal with it
				if (characters[i].PixelCoord.Height > 0)
				{
					botGap++;
				}

				// There's a gap between the top and the start of the character
				else
				{
					topGap++;
				}
			}

		}

		// If the column was empty, restart the width counter and advance
		// the character iterator
		if (empty == true)
		{
			printf("I:%u\tW:%u H:%u\n", i, characters[i].PixelCoord.Width, characters[i].PixelCoord.Height);

			oldX	= startX;       // What was I even going to do here?
			startX	= x + 1;        // Start of the next character
			i++;
		}

		// Reset the state for the next column and accumulate the info
		else if (empty == false)
		{
			empty = true;

			characters[i].PixelCoord.Width	+= 1;
			characters[i].UVCoord.X			= (f32)startX / (f32)header.Width;
			characters[i].UVCoord.Width		= (f32)x / (f32)header.Width;
		}
	}


	while (!GetAsyncKeyState(VK_ESCAPE))
	{
		BitBlt(hdc, 0, 0, header.Width, header.Height, hdcMem, 0, 0, SRCCOPY);
	}

	return 0;
}
#pragma once
#include "Renderer\Backend\RendererBase.h"
#include <Windows.h>

class RendererGL : public RendererBase
{
public:
	RendererGL	();
	~RendererGL	();

	bool Create		(WindowHandle windowHandle, u32 width, u32 height);
	bool Destroy	();

	void BeingFrame	(); // Rendering setup, like texture binding, etc
	void EndFrame	();

	void Clear		();
	void Present	();	// Just flip buffers

private:
	// Windows specific stuff
	HDC		hdc;
	HGLRC	context;	
};

typedef RendererGL Renderer;
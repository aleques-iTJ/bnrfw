#pragma once
#include "Extern\gl\glcorearb.h"
#include "Extern\gl\wglext.h"

//PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB; // This is used early on in creating the renderer!

PFNGLCLEARPROC			glClear;

PFNGLDISABLEPROC		glDisable;
PFNGLENABLEPROC			glEnable;

PFNGLCULLFACEPROC		glCullFace;
PFNGLDELETETEXTURESPROC	glDeleteTextures;
PFNGLDEPTHFUNCPROC		glDepthFunc;
PFNGLDEPTHMASKPROC		glDepthMask;
PFNGLDEPTHRANGEPROC		glDepthRange;

PFNGLGENBUFFERSPROC		glGenBuffers;
PFNGLGENTEXTURESPROC	glGenTextures;

PFNGLBINDTEXTURESPROC	glBindTextures;





void InitGLPointers()
{	
	glClear				= (PFNGLCLEARPROC)			wglGetProcAddress("glClear");
	glDisable			= (PFNGLDISABLEPROC)		wglGetProcAddress("glDisable");
	glEnable			= (PFNGLENABLEPROC)			wglGetProcAddress("glEnable");

	glCullFace			= (PFNGLCULLFACEPROC)		wglGetProcAddress("glCullFace");
	glDeleteTextures	= (PFNGLDELETETEXTURESPROC)	wglGetProcAddress("glDeleteTextures");
	glDepthFunc			= (PFNGLDEPTHFUNCPROC)		wglGetProcAddress("glDepthFunc");
	glDepthMask			= (PFNGLDEPTHMASKPROC)		wglGetProcAddress("glDepthMask");
	glDepthRange		= (PFNGLDEPTHRANGEPROC)		wglGetProcAddress("glDepthRange");

	glGenBuffers		= (PFNGLGENBUFFERSPROC)		wglGetProcAddress("glDepthRange");
	glGenTextures		= (PFNGLGENTEXTURESPROC)	wglGetProcAddress("glDepthRange");

	glBindTextures		= (PFNGLBINDTEXTURESPROC)	wglGetProcAddress("glDepthRange");
}
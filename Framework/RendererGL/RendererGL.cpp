#pragma once
#include "bnrFW\Video\RendererGL.h"
#include "bnrFW\Video\VideoEnums.h"

#include "bnrFW\Video\FunctionsGL.h"

RendererGL::RendererGL()
{

}

RendererGL::~RendererGL()
{
	Destroy();
}

bool RendererGL::Create(WindowHandle windowHandle, u32 width, u32 height)
{
	// Just try to grab the foreground window if the window handle is null
	// I'm too lazy to handle this properly atm
	if (windowHandle == nullptr)
		windowHandle = GetForegroundWindow();

	PIXELFORMATDESCRIPTOR pfd = { 0 };
	pfd.nSize			= sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion		= 1;
	pfd.dwFlags			= PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL;
	pfd.iPixelType		= PFD_TYPE_RGBA;
	pfd.cColorBits		= 24;
	pfd.cAlphaBits		= 8;
	pfd.cDepthBits		= 24;
	pfd.cStencilBits	= 8;

	hdc = GetDC(static_cast<HWND>(windowHandle));
	u32 fmt = ChoosePixelFormat(hdc, &pfd);
	u32 res = SetPixelFormat(hdc, fmt, &pfd);	

	// Set up a dummy context
	// OGL must be initilized to actually grab the function we want to create the REAL context
	HGLRC dummy = wglCreateContext(hdc);
	if (dummy)
	{
		// Ok, have a valid context, make it current and grab the wglCreateContextAttribsARB pointer
		wglMakeCurrent(hdc, dummy);

		s32 attribs[] =
		{
			WGL_CONTEXT_MAJOR_VERSION_ARB	, 3,
			WGL_CONTEXT_MINOR_VERSION_ARB	, 3,
			WGL_CONTEXT_FLAGS_ARB			, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
			WGL_CONTEXT_PROFILE_MASK_ARB	, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
			0
		};

		// Save the real context, obliterate the temp, and THEN set the real one!
		PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC) wglGetProcAddress("wglCreateContextAttribsARB");		
		context = wglCreateContextAttribsARB(hdc, 0, attribs);

		if (context)
		{
			InitGLPointers();

			wglMakeCurrent(nullptr, nullptr);
			wglDeleteContext(dummy);
			wglMakeCurrent(hdc, context);

			return true;
		}
	}

	return false;
}

bool RendererGL::Destroy()
{

	return true;
}

void RendererGL::Clear()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void RendererGL::Present()
{
	SwapBuffers(hdc);
}
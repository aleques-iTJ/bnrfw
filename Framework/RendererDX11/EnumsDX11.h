#pragma once
#include <d3d11.h>

enum class Format
{
	// Typical texture formats
	Unknown			= 0,							// This is probably an error!
	RGBA			= DXGI_FORMAT_R8G8B8A8_UNORM,
	BGRA			= DXGI_FORMAT_B8G8R8A8_UNORM,
	DXT1			= DXGI_FORMAT_BC1_UNORM,
	DXT3			= DXGI_FORMAT_BC3_UNORM,
	DXT5			= DXGI_FORMAT_BC5_UNORM,
	Depth			= DXGI_FORMAT_D32_FLOAT,
	Stencil			= 0,							// Does an explict stencil format exist?
	DepthStencil	= DXGI_FORMAT_D24_UNORM_S8_UINT,
	RenderTarget	= 1 << 30,						// What was I even going for here?

	// Index buffer type
	Indicies16Bit	= DXGI_FORMAT_R16_UINT,
	Indicies32Bit	= DXGI_FORMAT_R32_UINT,

	// Input layout component type
	Vector1			= DXGI_FORMAT_R32_FLOAT,
	Vector2			= DXGI_FORMAT_R32G32_FLOAT,
	Vector3			= DXGI_FORMAT_R32G32B32_FLOAT,
	Vector4			= DXGI_FORMAT_R32G32B32A32_FLOAT
};

enum class TextureDimension
{
	OneDimensional		= 1,	// This is tricky with DX...
	TwoDimensional		= 2,	// the flags are separate for Depth stencil and render targets!
	ThreeDimensional	= 3		// RTV need to have 1 added to them!
};

enum class FilterMode
{
	Nearest		= D3D11_FILTER_MIN_MAG_MIP_POINT,
	Trilinear	= D3D11_FILTER_MIN_MAG_MIP_LINEAR,
	Anisotropic = D3D11_FILTER_ANISOTROPIC
};

enum class AddressMode
{
	Repeat	= D3D11_TEXTURE_ADDRESS_WRAP,
	Mirror	= D3D11_TEXTURE_ADDRESS_MIRROR,
	Clamp	= D3D11_TEXTURE_ADDRESS_CLAMP,
};

enum class UsagePattern		// The expected usage pattern
{
	Default		= D3D11_USAGE_DEFAULT,
	Immutable	= D3D11_USAGE_IMMUTABLE,
	Static		= D3D11_USAGE_DEFAULT,
	Dynamic		= D3D11_USAGE_DYNAMIC,
	Staging		= D3D11_USAGE_STAGING
};

enum class BindFlags	// What it's bound as
{
	// We now just ignore the VS/PS distinction and bind both
	Unknown			= 0, // Invalid, this should blow up!
	VertexBuffer	= D3D11_BIND_VERTEX_BUFFER,
	IndexBuffer		= D3D11_BIND_INDEX_BUFFER,
	ConstantBuffer	= D3D11_BIND_CONSTANT_BUFFER,
	ShaderResource	= D3D11_BIND_SHADER_RESOURCE,
	StreamOutput	= D3D11_BIND_STREAM_OUTPUT,
	RenderTarget	= D3D11_BIND_RENDER_TARGET,
	DepthStencil	= D3D11_BIND_DEPTH_STENCIL,
	UnorderedAccess = D3D11_BIND_UNORDERED_ACCESS
};

enum class ReadWriteAccess
{
	Default		= 0,
	WriteAccess = D3D11_CPU_ACCESS_WRITE,
	ReadAccess	= D3D11_CPU_ACCESS_READ
};

enum class DepthWriteMask
{
	Disable	= D3D11_DEPTH_WRITE_MASK_ZERO,
	Enable	= D3D11_DEPTH_WRITE_MASK_ALL
};

enum class CompareFunc
{
	Never			= D3D11_COMPARISON_NEVER,
	Less			= D3D11_COMPARISON_LESS,
	Equal			= D3D11_COMPARISON_EQUAL,
	LessEqual		= D3D11_COMPARISON_LESS_EQUAL,
	Greate			= D3D11_COMPARISON_GREATER,
	NotEqual		= D3D11_COMPARISON_NOT_EQUAL,
	GreaterEqual	= D3D11_COMPARISON_GREATER_EQUAL,
	Always			= D3D11_COMPARISON_ALWAYS
};

enum class StencilOp
{
	Keep		= D3D11_STENCIL_OP_KEEP,
	Zero		= D3D11_STENCIL_OP_ZERO,
	Replace		= D3D11_STENCIL_OP_REPLACE,
	IncreaseSat	= D3D11_STENCIL_OP_INCR_SAT,
	DecreaseSat	= D3D11_STENCIL_OP_DECR_SAT,
	Invert		= D3D11_STENCIL_OP_INVERT,
	Increase	= D3D11_STENCIL_OP_INCR,
	Decrease	= D3D11_STENCIL_OP_DECR
};

enum class FillMode
{
	Wireframe	= D3D11_FILL_WIREFRAME,
	Solid		= D3D11_FILL_SOLID
};

enum class CullMode
{
	None	= D3D11_CULL_NONE,
	Front	= D3D11_CULL_FRONT,
	Back	= D3D11_CULL_BACK
};

enum class PrimitiveType
{
	Point			= D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
	LineList		= D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
	LineStrip		= D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP,
	TriangleList	= D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
	TriangleStrip	= D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP
};

enum ShaderType
{
	//Unknown			= 0, // Probably an error...
	VertexShader	= 1 << 1,
	PixelShader		= 1 << 2,
	GeometryShader	= 1 << 3,
};
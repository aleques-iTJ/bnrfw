#pragma once
#include "RendererDX11\Backend\ShaderDX11.h"
#include "RendererDX11\Backend\RendererDX11.h"

#include "Core\Globals.h"
#include <D3Dcompiler.h>

void ShaderDX11::LoadFromFile(const String& path, bool compile /* = true */)
{
	file = g_FileSystem->OpenFile(path, FileAccess::Read);
	g_FileSystem->ReadFile(file);

	//rawShaderText.Set((char*)file->GetDataPtr());

	if (compile)
		Compile();
}

void ShaderDX11::LoadFromMemory(void* ptr)
{
	ptr = 0; // SHUT UP WITH THE WARNING
	assert("NOT IMPLEMENTED");
}

//----------------------------------------------------------------

void ShaderDX11::Compile()
{
	// Actually compile the shader first

#ifdef _DEBUG
	 HRESULT	hr	= 0;
	 ID3DBlob*	err = nullptr;

	 hr = D3DCompile((char*)file->GetDataPtr(), file->GetSize(), nullptr, nullptr, nullptr, "vsMain", "vs_4_0",
					 D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &vsBuffer, &err);

	 hr = D3DCompile((char*)file->GetDataPtr(), file->GetSize(), nullptr, nullptr, nullptr, "psMain", "ps_4_0",
					D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &psBuffer, &err);
#else
	D3DCompile((char*)file->GetDataPtr(), file->GetSize(), nullptr, nullptr, nullptr,
					"vsMain", "vs_4_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &vsBuffer, nullptr);

	D3DCompile((char*)file->GetDataPtr(), file->GetSize(), nullptr, nullptr, nullptr,
		"psMain", "ps_4_0", D3DCOMPILE_OPTIMIZATION_LEVEL3, 0, &psBuffer, nullptr);
#endif
}

void ShaderDX11::Activate()
{
	renderer->activeState.vertexShader	= this;
	renderer->activeState.pixelShader	= this;
}
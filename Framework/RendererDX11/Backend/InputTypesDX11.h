#pragma once
#include "Core\Types.h"
#include <d3d11.h>

namespace InputLayout
{
	enum class Type
	{
		PositionColor,
		PositionColorTexcoord
	};

//----------------------------------------------------------------

	// Mostly intended for 2D stuff like sprites
	struct PCTVertex
	{
		f32	x, y, z;	// 12 bytes, possibly the literal position in pixels
		f32 r, g, b;	// 24 bytes, color to be modulated against
		f32	u, v;		// 32 bytes, tex coordinate
	};

	extern ID3D11InputLayout*		PCTPtr;
	extern D3D11_INPUT_ELEMENT_DESC	PCTDesc[3];
}
#pragma once
#include "Renderer\Backend\RendererBase.h"
#include "RendererDX11\Backend\StateCacheDX11.h"
#include "RendererDX11\Backend\InputTypesDX11.h"

#include "RendererDX11\EnumsDX11.h"
#include <d3d11.h>

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

class BNR_API RendererDX11 : public RendererBase
{
	friend struct ResourceBase;

	friend struct BufferDX11;
	friend struct TextureDX11;
	friend struct ShaderDX11;

	friend struct DepthStencilStateDX11;
	friend struct RasterStateDX11;
	friend struct SamplerStateDX11;

//----------------------------------------------------------------

public:
	RendererDX11	();
	~RendererDX11	();

	// Setup
	bool Create					(WindowHandle handle, u32 width, u32 height);
	bool Destroy				();

	// ... Renderer stuff
	void PreRender				();			// Rendering setup, like texture binding, etc
	void PostRender				() const;
	void Clear					() const;
	void Present				() const;	// Just flip buffers

	// Resource creation
	void CreateBuffer			(BufferDX11& buffer)	const;
	void CreateTexture			(TextureDX11& texture)	const;
	void CreateShader			(ShaderDX11& shader)	const;

	// State creation
	void CreateDepthStencilState(DepthStencilStateDX11& state)	const;
	void CreateRasterState		(RasterStateDX11& state)		const;
	void CreateSamplerState		(SamplerStateDX11& state)		const;

	// Draw calls
	void Draw					(u32 vertexCount, u32 offset = 0);
	void DrawIndexed			(u32 indexCount, u32 indexOffset = 0, u32 vertexOffset = 0);

	// Memory
	void Map					(const ResourceBase& resource, u32 index, u32 flags, void** ptr)			const;
	void Unmap					(const ResourceBase& resource, u32 index = 0)								const;
	void MapThenUnmap			(const ResourceBase& resource, u32 index, u32 flags, void* ptr, u32 size)	const; // Just convenience
	void UpdateSubresource		(const ResourceBase& resource, u32 index, void* ptr)						const;

	// Other
	void GetViewport			(Viewport& viewport) const;
	void SetViewport			(const Viewport& viewport);
	void SetPrimitiveType		(PrimitiveType primitiveType);

//----------------------------------------------------------------

private:
	// Some raw D3D stuff
	ComPtr<IDXGISwapChain>		swapChain;
	ComPtr<ID3D11Device>		device;
	ComPtr<ID3D11DeviceContext>	context;

	TextureDX11*				depthBuffer;
	TextureDX11*				backBuffer;

	// State stuff
	StateBlockDX11				activeState;
	StateBlockDX11				previousState;

	// State filtering and resource binding management
	// This is where resources are actually bound to the pipeline!
	void _filterRedundantBuffers		();
	void _filterRedundantTextures		();
	void _filterRedundantRenderStates	();
	void _filterRedundantShaders		();
};
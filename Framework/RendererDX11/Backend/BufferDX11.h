#pragma once
#include "Renderer\Backend\BufferBase.h"
#include "RendererDX11\Backend\RendererDX11.h"

struct BNR_API BufferDX11 : public BufferBase
{
	friend class RendererDX11;

//----------------------------------------------------------------

public:
	BufferDX11::BufferDX11(bool defaults = false) :
		buf			(nullptr),
		srv			(nullptr)
	{
		if (defaults == true)
			SetDefaults();
	}

	BufferDX11::~BufferDX11()
	{

	}

//----------------------------------------------------------------

	void SetDefaults()
	{
		RWAccess	= ReadWriteAccess::Default;
		BindAs		= BindFlags::Unknown;
		Usage		= UsagePattern::Default;
		MiscFlags	= 0;

		Size		= 0;
		Stride		= 0;
		Offset		= 0;
	};


	void Activate(u32 slot)
	{
		switch (BindAs)
		{
		case BindFlags::VertexBuffer:
			renderer->activeState.vertexBuffers.Insert(slot, this);
			break;

		case BindFlags::IndexBuffer:
			renderer->activeState.indexBuffer = this;
			break;

		case BindFlags::ConstantBuffer:
			renderer->activeState.constantBuffers.Insert(slot, this);
			break;
		}
	}

//----------------------------------------------------------------

private:
	RendererDX11* renderer;

	ComPtr<ID3D11Buffer>				buf;
	ComPtr<ID3D11ShaderResourceView>	srv;
};
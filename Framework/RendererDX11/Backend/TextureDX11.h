#pragma once
#include "RendererDX11\EnumsDX11.h"

#include "Renderer\Backend\TextureBase.h"
#include "RendererDX11\Backend\RendererDX11.h"

struct BNR_API TextureDX11 : public TextureBase
{
	friend class RendererDX11;

//----------------------------------------------------------------

public:
	TextureDX11::TextureDX11(bool defaults = false)
	{
		if (defaults == true)
			SetDefaults();
	}

	TextureDX11::~TextureDX11()
	{

	}

//----------------------------------------------------------------

	void SetDefaults()
	{
		// Base Resource
		//Subresource	= nullptr;
		Format			= Format::RGBA;					// Unknown
		RWAccess		= ReadWriteAccess::Default;		
		BindAs			= BindFlags::ShaderResource;	// Unknwon
		Usage			= UsagePattern::Static;
		MiscFlags		= 0;
		Size			= 0;
		Stride			= 0;

		// Base Texture
		Dimension		= TextureDimension::TwoDimensional;
		Filter			= FilterMode::Trilinear;
		WrapMode		= AddressMode::Repeat;
		Width			= 1;
		Height			= 1;
		Depth			= 0;

		NumFrames		= 1;
		FrameRate		= 0;
		MipMapCount		= 1;
	}

	void Activate(u32 slot)
	{
		renderer->activeState.textures.Insert(slot, this);
	}

//----------------------------------------------------------------

private:
	RendererDX11*	renderer;

	ComPtr<ID3D11Texture2D>				tex;

	ComPtr<ID3D11ShaderResourceView>	srv;
	ComPtr<ID3D11RenderTargetView>		rtv;
	ComPtr<ID3D11DepthStencilView>		dsv;
};
#pragma once
#include "RendererDX11\EnumsDX11.h"

#include "Renderer\Backend\DepthStencilStateBase.h"
#include "RendererDX11\Backend\RendererDX11.h"

struct BNR_API DepthStencilStateDX11 : public DepthStencilStateBase
{
	friend class RendererDX11;

//----------------------------------------------------------------

public:
	DepthStencilStateDX11::DepthStencilStateDX11(bool defaults = false) :
		state(nullptr)
	{
		if (defaults == true)
			SetDefaults();
	}

	DepthStencilStateDX11::~DepthStencilStateDX11()
	{

	}

//----------------------------------------------------------------

	void SetDefaults()
	{
			DepthEnable						= true;
			DepthWrite						= DepthWriteMask::Disable;
			DepthFunc						= CompareFunc::Less;
			StencilEnable					= false;
			StencilReadMask					= 0xFF;
			StencilWriteMask				= 0xFF;
			FrontFace.StencilFunc			= CompareFunc::Always;
			FrontFace.StencilDepthFailOp	= StencilOp::Keep;
			FrontFace.StencilPassOp			= StencilOp::Keep;
			FrontFace.StencilFailOp			= StencilOp::Keep;
			BackFace.StencilFunc			= CompareFunc::Always;
			BackFace.StencilDepthFailOp		= StencilOp::Keep;
			BackFace.StencilPassOp			= StencilOp::Keep;
			BackFace.StencilFailOp			= StencilOp::Keep;
	}

	void Activate()
	{
		renderer->activeState.depthStencilState = this;
	}

//----------------------------------------------------------------

private:
	RendererDX11* renderer;

	ComPtr<ID3D11DepthStencilState> state;
};
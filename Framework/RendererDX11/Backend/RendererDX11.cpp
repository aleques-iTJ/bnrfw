#pragma once
#include "RendererDX11\Backend\RendererDX11.h"

#include "Renderer\Backend\ResourceBase.h"
#include "RendererDX11\Backend\BufferDX11.h"
#include "RendererDX11\Backend\TextureDX11.h"
#include "RendererDX11\Backend\ShaderDX11.h"

#include "RendererDX11\Backend\DepthStencilStateDX11.h"
#include "RendererDX11\Backend\RasterStateDX11.h"
#include "RendererDX11\Backend\SamplerStateDX11.h"

//#include "Core\Memory\Memory.h"

RendererDX11::RendererDX11()
{

}

RendererDX11::~RendererDX11()
{
	Destroy();
}

//----------------------------------------------------------------

bool RendererDX11::Create(WindowHandle handle, u32 width, u32 height)
{
	// Just try to grab the foreground window if the window handle is null
	if (handle == nullptr)
		windowHandle = GetForegroundWindow();
	else
		windowHandle = handle;

	// The swap chain isn't wrapped
	DXGI_SWAP_CHAIN_DESC scd;
	scd.BufferCount							= 1;
	scd.BufferDesc.Width					= width;
	scd.BufferDesc.Height					= height;
	scd.BufferDesc.Format					= DXGI_FORMAT_R8G8B8A8_UNORM;
	scd.BufferDesc.RefreshRate.Numerator	= 60;										// This is WRONG, should be properly enumerated!
	scd.BufferDesc.RefreshRate.Denominator	= 1;
	scd.BufferDesc.Scaling					= DXGI_MODE_SCALING_UNSPECIFIED;
	scd.BufferUsage							= DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scd.OutputWindow						= windowHandle;
	scd.SampleDesc.Count					= 1;
	scd.SampleDesc.Quality					= 0;
	scd.Windowed							= true;										// Switch later...	
	scd.Flags								= DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;	// Always start in a windowed state
	scd.SwapEffect							= DXGI_SWAP_EFFECT_DISCARD;

	D3D_FEATURE_LEVEL fl = D3D_FEATURE_LEVEL_10_0;
#ifdef _DEBUG	
	HRESULT hr = D3D11CreateDeviceAndSwapChain(0, D3D_DRIVER_TYPE_HARDWARE, 0, D3D11_CREATE_DEVICE_DEBUG, &fl, 1, D3D11_SDK_VERSION, &scd, &swapChain, &device, 0, &context);
#else
	HRESULT hr = D3D11CreateDeviceAndSwapChain(0, D3D_DRIVER_TYPE_HARDWARE, 0, D3D11_CREATE_DEVICE_SINGLETHREADED, &fl, 1, D3D11_SDK_VERSION, &scd, &swapChain, &device, 0, &context);
#endif

	if (hr == S_OK) // We're okay!
	{
		// Some setup of our own
		backBuffer	= new TextureDX11;
		depthBuffer	= new TextureDX11;
		memset(&frameInfo, 0, sizeof(FrameInfo));

		// Setup the state cache
		memset(&activeState, 0, sizeof(StateBlockDX11));
		memset(&previousState, 0, sizeof(StateBlockDX11));

		// Start getting set up to actually... do D3D things
		swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**) &backBuffer->tex);
		device->CreateRenderTargetView(backBuffer->tex, 0, &backBuffer->rtv);

		backBuffer->SetDefaults();
		depthBuffer->SetDefaults();
		depthBuffer->Width		= width;
		depthBuffer->Height		= height;
		depthBuffer->Format		= Format::DepthStencil;
		depthBuffer->BindAs		= BindFlags::DepthStencil;
		backBuffer->BindAs		= BindFlags::RenderTarget;

		// GetBuffer() has already done the rest internally for the backbuffer
		// This is why nothing is really set for its texture
		this->CreateTexture(*depthBuffer);

		// Just cheat this time and bind it immediately rather than deferring it to PreRender()
		ID3D11RenderTargetView* temp = backBuffer->rtv.GetInterface();
		this->context->OMSetRenderTargets(1, &temp, depthBuffer->dsv);

		Viewport vp;
		vp.Width	= static_cast<f32>(width);
		vp.Height	= static_cast<f32>(height);
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		this->SetViewport(vp);

		// We've probably managed to survive if we're this far, so call it a success
		return true;
	}

	return false;
}

bool RendererDX11::Destroy()
{
	delete depthBuffer;
	delete backBuffer;

	// The DX11 resources should release themselves ... HOPEFULLY
	// Don't be surprised if something leaks ATM!
	return true;
}

//----------------------------------------------------------------

void RendererDX11::PreRender()
{
	// Obliterate the per frame stats, time to start anew
	memset(&frameInfo, 0, sizeof(FrameInfo));

	// am i doin this rite
	// http://lspiroengine.com/?p=570

	// Does what it says on the tin, sets up for rendering
	// This includes stuff like taking care of state filtering, etc...
	_filterRedundantBuffers();
	_filterRedundantTextures();
	_filterRedundantRenderStates();
	_filterRedundantShaders();
}

void RendererDX11::PostRender() const
{

}

void RendererDX11::Clear() const
{
	const f32 color[4] = { 0.0f, 0.3f, 0.0f, 0.0f };

	context->ClearRenderTargetView(backBuffer->rtv, color);
	context->ClearDepthStencilView(depthBuffer->dsv, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}

void RendererDX11::Present() const
{
	swapChain->Present(r_vsync.Get(), 0);
}

//----------------------------------------------------------------

void RendererDX11::CreateBuffer(BufferDX11& buffer) const
{
	D3D11_BUFFER_DESC desc;
	desc.ByteWidth				= buffer.Size;
	desc.CPUAccessFlags			= static_cast<UINT>(buffer.RWAccess);
	desc.MiscFlags				= 0;
	desc.StructureByteStride	= 0;
	desc.Usage					= static_cast<D3D11_USAGE>(buffer.Usage);
	desc.BindFlags				= static_cast<UINT>(buffer.BindAs);

	// Must have the data already prepared if it's Immutable
	if (buffer.Usage == UsagePattern::Immutable)
	{
		D3D11_SUBRESOURCE_DATA srd;
		srd.pSysMem				= buffer.Subresources[0].Data;
		srd.SysMemPitch			= buffer.Subresources[0].Stride;
		srd.SysMemSlicePitch	= 0;

		device->CreateBuffer(&desc, &srd, &buffer.buf);
	}

	else
		device->CreateBuffer(&desc, nullptr, &buffer.buf);

	buffer.nativeResource = buffer.buf;
	// Give it our renderer pointer
	buffer.renderer = const_cast<RendererDX11*>(this);
}

void RendererDX11::CreateTexture(TextureDX11& texture) const
{
	D3D11_TEXTURE2D_DESC desc;

	// Do some ugly translation...
	desc.ArraySize			= 1;
	desc.MipLevels			= texture.MipMapCount;
	desc.MiscFlags			= 0;
	desc.SampleDesc			= { 1, 0 };
	desc.CPUAccessFlags		= static_cast<UINT>(texture.RWAccess);
	desc.Format				= static_cast<DXGI_FORMAT>(texture.Format);
	desc.Height				= texture.Height;
	desc.Width				= texture.Width;
	desc.Usage				= static_cast<D3D11_USAGE>(texture.Usage);
	desc.BindFlags			= static_cast<UINT>(texture.BindAs);

	// Use any existing sub resources as initial data
	u32 numSubResources = texture.Subresources.GetSize();
	if (numSubResources > 0)
	{
		for (u32 i = 0; i < numSubResources; i++)
		{
			D3D11_SUBRESOURCE_DATA* srd = new D3D11_SUBRESOURCE_DATA[numSubResources];
			srd[i].pSysMem				= texture.Subresources[i].Data;
			srd[i].SysMemPitch			= texture.Subresources[i].Stride;
			srd[i].SysMemSlicePitch		= 0;

			device->CreateTexture2D(&desc, srd, &texture.tex);
			delete srd;
		}		
	}

	else
		device->CreateTexture2D(&desc, nullptr, &texture.tex);

	// Create views 
	if (desc.BindFlags & static_cast<UINT>(BindFlags::ShaderResource))
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.Format						= desc.Format;
		srvDesc.ViewDimension				= D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels			= desc.MipLevels;
		srvDesc.Texture2D.MostDetailedMip	= 0;

		device->CreateShaderResourceView(texture.tex, &srvDesc, &texture.srv);
	}

	if (desc.BindFlags & static_cast<UINT>(BindFlags::RenderTarget))
	{
		D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
		rtvDesc.Format				= desc.Format;
		rtvDesc.ViewDimension		= D3D11_RTV_DIMENSION_TEXTURE2D;
		rtvDesc.Texture2D.MipSlice	= 0;

		device->CreateRenderTargetView(texture.tex, &rtvDesc, &texture.rtv);
	}

	if (desc.BindFlags & static_cast<UINT>(BindFlags::DepthStencil))
	{
		D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
		dsvDesc.Format				= desc.Format;
		dsvDesc.ViewDimension		= D3D11_DSV_DIMENSION_TEXTURE2D;
		dsvDesc.Texture2D.MipSlice	= 0;
		dsvDesc.Flags				= 0;

		device->CreateDepthStencilView(texture.tex, &dsvDesc, &texture.dsv);
	}

	// Give it our renderer pointer
	texture.renderer = const_cast<RendererDX11*>(this);
}

void RendererDX11::CreateShader(ShaderDX11& shader)	const
{
	// The shader should of already loaded the file and tried to compile it
	// static_cast is disgustingly difficult to read here, barf
	if ((int)shader.ShaderType & (int)ShaderType::VertexShader)
		device->CreateVertexShader(shader.vsBuffer->GetBufferPointer(), shader.vsBuffer->GetBufferSize(), 0, &shader.vs);

	if ((int)shader.ShaderType & (int)ShaderType::PixelShader)
		device->CreatePixelShader(shader.psBuffer->GetBufferPointer(), shader.psBuffer->GetBufferSize(), 0, &shader.ps);


	if (shader.InputType == InputLayout::Type::PositionColorTexcoord)
	{
		if (InputLayout::PCTPtr == nullptr)
		{
			device->CreateInputLayout(InputLayout::PCTDesc, 3, shader.vsBuffer->GetBufferPointer(),
									  shader.vsBuffer->GetBufferSize(), &InputLayout::PCTPtr);

			context->IASetInputLayout(InputLayout::PCTPtr);

			shader.layout = InputLayout::PCTPtr;
		}		
	}

	// Give it our renderer pointer
	shader.renderer = const_cast<RendererDX11*>(this);
}

//----------------------------------------------------------------

void RendererDX11::CreateDepthStencilState(DepthStencilStateDX11& state) const
{
	// BROKEN
	D3D11_DEPTH_STENCIL_DESC* desc = reinterpret_cast<D3D11_DEPTH_STENCIL_DESC*>(&state);

	

	device->CreateDepthStencilState(desc, &state.state);

	// Give it our renderer pointer
	state.renderer = const_cast<RendererDX11*>(this);
}

void RendererDX11::CreateRasterState(RasterStateDX11& state) const
{

	assert("NOT IMPLEMENTED");

	// Give it our renderer pointer
	//state.renderer = const_cast<RendererDX11*>(this);
}

void RendererDX11::CreateSamplerState(SamplerStateDX11& state) const
{
	D3D11_SAMPLER_DESC* desc = reinterpret_cast<D3D11_SAMPLER_DESC*>(&state.Filter);
	device->CreateSamplerState(desc, &state.state);

	// Give it our renderer pointer
	state.renderer = const_cast<RendererDX11*>(this);
}

//----------------------------------------------------------------

void RendererDX11::Draw(u32 vertexCount, u32 offset)
{
	context->Draw(vertexCount, offset);
	frameInfo.DrawCalls++;
}

void RendererDX11::DrawIndexed(u32 indexCount, u32 indexOffset, u32 vertexOffset)
{
	context->DrawIndexed(indexCount, indexOffset, vertexOffset);
	frameInfo.DrawCallsIndexed++;
}

//----------------------------------------------------------------

void RendererDX11::Map(const ResourceBase& resource, u32 index, u32 flags, void** ptr) const
{
	D3D11_MAPPED_SUBRESOURCE temp;
	context->Map(resource.nativeResource, index, static_cast<D3D11_MAP>(flags), 0, &temp);
	*ptr = temp.pData;
}

void RendererDX11::Unmap(const ResourceBase& resource, u32 index) const
{
	context->Unmap(resource.nativeResource, index);
}

void RendererDX11::MapThenUnmap(const ResourceBase& resource, u32 index, u32 flags, void* ptr, u32 size) const
{
	D3D11_MAPPED_SUBRESOURCE temp = { 0 };
	context->Map(resource.nativeResource, index, static_cast<D3D11_MAP>(flags), 0, &temp);
	memcpy((char*)temp.pData, (char*)ptr, size);	
	context->Unmap(resource.nativeResource, index);
}

void RendererDX11::UpdateSubresource(const ResourceBase& resource, u32 index, void* ptr) const
{
	UNREFERENCED_PARAMETER(index);

	assert("NOT IMPLEMENTED");
	//context->UpdateSubresource(resource.nativeResource, 0, nullptr, ptr, 0, 0);
}

//----------------------------------------------------------------

void RendererDX11::GetViewport(Viewport& viewport) const
{
	u32 num = 1;
	context->RSGetViewports(&num, reinterpret_cast<D3D11_VIEWPORT*>(&viewport));
}

void RendererDX11::SetViewport(const Viewport& viewport)
{
	context->RSSetViewports(1, reinterpret_cast<const D3D11_VIEWPORT*>(&viewport));
}

void RendererDX11::SetPrimitiveType(PrimitiveType primitiveType)
{
	context->IASetPrimitiveTopology(static_cast<D3D11_PRIMITIVE_TOPOLOGY>(primitiveType));
}

//----------------------------------------------------------------

void RendererDX11::_filterRedundantBuffers()
{
	// Need to extract native types into an array to bind as a batch
	// Vertex buffers first
	ID3D11Buffer*	vbBunch  [D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT];
	u32				vbStrides[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT];
	u32				vbOffsets[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT];

	// Constant buffers
	ID3D11Buffer*	cbBunch	 [D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT];

	
	// Vertex buffers
	u32 bufIndex	= 0;	// Where we are in the array of buffer resources
	u32 bufToBind	= 0;	// Number of buffers that want to be bound
	u32 maxBindable = max(activeState.vertexBuffers.GetSize(), previousState.vertexBuffers.GetSize()); // Max possible that can be changed

	for (u32 i = 0; i < maxBindable; i++)
	{
		// Check if the active object that wants to be bound is actually different than the last
		if (activeState.vertexBuffers[i] != previousState.vertexBuffers[i])
		{
			// It's different - replace / update it
			bufToBind++;
			previousState.vertexBuffers[i] = activeState.vertexBuffers[i];

			// Start accumulating the data into what D3D expects
			vbBunch  [i] = activeState.vertexBuffers[i]->buf;
			vbStrides[i] = activeState.vertexBuffers[i]->Stride;
			vbOffsets[i] = activeState.vertexBuffers[i]->Offset;
		}

		else
		{
			// Bumped into a resource that's the same as what's already bound
			// Need to bind what's been accumulated so far
			if (bufToBind > 0)
			{
				context->IASetVertexBuffers(bufIndex, bufToBind, &vbBunch[bufIndex], &vbStrides[bufIndex], &vbOffsets[bufIndex]);
				frameInfo.BufferBinds += bufToBind;
			}

			bufIndex	= i + 1;	// Index of the next buffer after the bunch just bound
			bufToBind	= 0;		// Nothing else to immediately bind
		}
	}

	// Wrap up - that means anything not yet bound, and / or left over
	if (bufToBind > 0)
	{
		context->IASetVertexBuffers(bufIndex, bufToBind, &vbBunch[bufIndex], &vbStrides[bufIndex], &vbOffsets[bufIndex]);
		frameInfo.BufferBinds += bufToBind;
	}

	// Index buffer
	if (activeState.indexBuffer != previousState.indexBuffer)
	{
		previousState.indexBuffer = activeState.indexBuffer;
		context->IASetIndexBuffer(activeState.indexBuffer->buf, static_cast<DXGI_FORMAT>(activeState.indexBuffer->Format), activeState.indexBuffer->Offset);

		frameInfo.BufferBinds += 1;
	}

	// Constant buffers
	bufIndex	= 0;
	bufToBind	= 0;
	maxBindable	= max(activeState.constantBuffers.GetSize(), previousState.constantBuffers.GetSize());

	for (u32 i = 0; i < maxBindable; i++)
	{
		if (activeState.constantBuffers[i] != previousState.constantBuffers[i])
		{
			bufToBind++;
			previousState.constantBuffers[i] = activeState.constantBuffers[i];

			cbBunch[i] = activeState.constantBuffers[i]->buf;
		}

		else
		{
			if (bufToBind > 0)
			{
				context->VSSetConstantBuffers(bufIndex, bufToBind, &cbBunch[i]);
				context->PSSetConstantBuffers(bufIndex, bufToBind, &cbBunch[i]);

				frameInfo.BufferBinds += bufToBind;
			}

			bufIndex	= i + 1;
			bufToBind	= 0;
		}
	}

	if (bufToBind > 0)
	{
		context->VSSetConstantBuffers(bufIndex, bufToBind, &cbBunch[bufIndex]);
		context->PSSetConstantBuffers(bufIndex, bufToBind, &cbBunch[bufIndex]);

		frameInfo.BufferBinds += bufToBind;
	}
}

void RendererDX11::_filterRedundantTextures()
{
	// Look above for comments, it's nearly the same idea
	u32 srIndex		= 0;
	u32 srToBind	= 0;
	u32 maxBindable = max(activeState.textures.GetSize(), previousState.textures.GetSize()); 

	ID3D11ShaderResourceView* srvBunch[D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT];

	for (u32 i = 0; i < maxBindable; i++)
	{
		if (activeState.textures[i] != previousState.textures[i])
		{
			srToBind++;
			previousState.textures[i] = activeState.textures[i];

			srvBunch[i] = activeState.textures[i]->srv;
		}

		else
		{
			if (srToBind > 0)
			{
				context->VSSetShaderResources(srIndex, srToBind, &srvBunch[srIndex]);
				context->PSSetShaderResources(srIndex, srToBind, &srvBunch[srIndex]);

				frameInfo.TextureSwitches += srToBind;
			}

			srIndex		= i + 1;
			srToBind	= 0;
		}
	}

	if (srToBind > 0)
	{
		context->VSSetShaderResources(srIndex, srToBind, &srvBunch[srIndex]);
		context->PSSetShaderResources(srIndex, srToBind, &srvBunch[srIndex]);

		frameInfo.TextureSwitches += srToBind;
	}
}

void RendererDX11::_filterRedundantRenderStates()
{
	u32 stateIndex	= 0;
	u32 stateToBind	= 0;
	u32 maxActive	= max(activeState.samplerStates.GetSize(), previousState.samplerStates.GetSize());

	ID3D11SamplerState* ssBunch[D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT];

	for (u32 i = 0; i < maxActive; i++)
	{
		if (activeState.samplerStates[i] != previousState.samplerStates[i])
		{
			stateToBind++;
			previousState.samplerStates[i] = activeState.samplerStates[i];

			ssBunch[i] = activeState.samplerStates[i]->state;
		}

		else
		{
			if (stateToBind > 0)
			{
				context->VSSetSamplers(stateIndex, stateToBind, &ssBunch[stateIndex]);
				context->PSSetSamplers(stateIndex, stateToBind, &ssBunch[stateIndex]);

				frameInfo.StateChanges += stateToBind;
			}

			stateIndex	= i + 1;
			stateToBind	= 0;
		}
	}

	if (stateToBind > 0)
	{
		context->VSSetSamplers(stateIndex, stateToBind, &ssBunch[stateIndex]);
		context->PSSetSamplers(stateIndex, stateToBind, &ssBunch[stateIndex]);

		frameInfo.StateChanges += stateToBind;
	}

//----------------------------------------------------------------

	// Raster
	if (activeState.rasterState != previousState.rasterState)
	{
		previousState.rasterState = activeState.rasterState;
		context->RSSetState(activeState.rasterState->state);

		frameInfo.StateChanges += 1;
	}

//----------------------------------------------------------------

	// Depth Stencil
	if (activeState.depthStencilState != previousState.depthStencilState)
	{
		previousState.depthStencilState = activeState.depthStencilState;
		context->OMSetDepthStencilState(activeState.depthStencilState->state, 0);

		frameInfo.StateChanges += 1;
	}
}

void RendererDX11::_filterRedundantShaders()
{
	// holy simple for a change...
	if (activeState.vertexShader != previousState.vertexShader)
	{
		previousState.vertexShader = activeState.vertexShader;
		context->VSSetShader(activeState.vertexShader->vs, nullptr, 0);

		frameInfo.ShaderSwitches += 1;
	}

	if (activeState.pixelShader != previousState.pixelShader)
	{
		previousState.pixelShader = activeState.pixelShader;
		context->PSSetShader(activeState.pixelShader->ps, nullptr, 0);

		frameInfo.ShaderSwitches += 1;
	}
}
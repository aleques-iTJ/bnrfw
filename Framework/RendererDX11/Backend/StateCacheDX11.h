#pragma once
#include "Core\Containers\FixedArray.h"

struct StateBlockDX11
{
	friend class RendererDX11;

	friend struct BufferDX11;
	friend struct TextureDX11;
	friend struct ShaderDX11;

	friend struct DepthStencilStateDX11;
	friend struct RasterStateDX11;
	friend struct SamplerStateDX11;	

//----------------------------------------------------------------

private:
	// State
	DepthStencilStateDX11*				depthStencilState;
	RasterStateDX11*					rasterState;
	FixedArray<SamplerStateDX11*, 16>	samplerStates;

	// Shaders
	ShaderDX11*							vertexShader;
	ShaderDX11*							pixelShader;

	// Buffers
	FixedArray<BufferDX11*, 16>			vertexBuffers;
	FixedArray<BufferDX11*, 14>			constantBuffers;
	BufferDX11*							indexBuffer;

	// Shader resources
	FixedArray<TextureDX11*, 16>		textures;
};
#pragma once
#include "RendererDX11\EnumsDX11.h"

#include "Renderer\Backend\RasterStateBase.h"
#include "RendererDX11\Backend\RendererDX11.h"

struct BNR_API RasterStateDX11 : public RasterStateBase
{
	friend class RendererDX11;

//----------------------------------------------------------------

public:
	RasterStateDX11::RasterStateDX11(bool defaults = false) :
		state(nullptr)
	{
		if (defaults == true)
			SetDefaults();
	}

	RasterStateDX11::~RasterStateDX11()
	{

	}

//----------------------------------------------------------------

	void SetDefaults()
	{
		FillMode				= FillMode::Solid;
		CullMode				= CullMode::Back;
		FrontCounterClockwise	= false;
		DepthBias				= 0;
		SlopeScaledDepthBias	= 0.0f;
		DepthBiasClamp			= 0.0f;
		DepthClipEnable			= true;
		ScissorEnable			= false;
		MultisampleEnable		= false;
		AntialiasedLineEnable	= false;
	}

	void Activate()
	{
		renderer->activeState.rasterState = this;
	}

//----------------------------------------------------------------

private:
	RendererDX11* renderer;

	ComPtr<ID3D11RasterizerState>	state;
};
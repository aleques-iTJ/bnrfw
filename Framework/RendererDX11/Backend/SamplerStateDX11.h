#pragma once
#include "RendererDX11\EnumsDX11.h"

#include "Renderer\Backend\SamplerStateBase.h"
#include "RendererDX11\Backend\RendererDX11.h"

struct BNR_API SamplerStateDX11 : public SamplerStateBase
{
	friend class RendererDX11;

//----------------------------------------------------------------

public:
	SamplerStateDX11::SamplerStateDX11(bool defaults = false) :
		state(nullptr)
	{
		if (defaults == true)
			SetDefaults();
	}

	SamplerStateDX11::~SamplerStateDX11()
	{

	}

//----------------------------------------------------------------

	void SetDefaults()
	{
		Filter			= FilterMode::Trilinear;
		AddressU		= AddressMode::Clamp;
		AddressV		= AddressMode::Clamp;
		AddressW		= AddressMode::Clamp;
		MipLODBias		= 0;
		MaxAnisotropy	= 1;
		ComparisonFunc	= CompareFunc::Never;
		BorderColor[0]	= 0.0f;
		BorderColor[1]	= 0.0f;
		BorderColor[2]	= 0.0f;
		BorderColor[3]	= 0.0f;
		MinLOD			= -1E+37f;
		MaxLOD			= 1E+37f;
	}

	void Activate(u32 slot)
	{
		renderer->activeState.samplerStates.Insert(slot, this);
	}

//----------------------------------------------------------------

private:
	RendererDX11* renderer;

	ComPtr<ID3D11SamplerState>	state;
};
#pragma once

// kool
struct BufferDX11;
struct TextureDX11;
struct ShaderDX11;

struct DepthStencilStateDX11;
struct RasterStateDX11;
struct SamplerStateDX11;

class  RendererDX11;

//----------------------------------------------------------------

typedef BufferDX11				Buffer;
typedef TextureDX11				Texture;
typedef ShaderDX11				Shader;

typedef DepthStencilStateDX11	DepthStencilState;
typedef RasterStateDX11			RasterState;
typedef SamplerStateDX11		SamplerState;

typedef RendererDX11			Renderer;
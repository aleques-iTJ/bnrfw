#pragma once
#include "Core\Utility\Cvar.h"
#pragma warning(disable : 4251)

//----------------------------------------------------------------

enum class ControlType
{
	Button,
	CheckBox,
	ListBox,
	ComboBox,
	TextBox,
	Label
};

class BNR_API WindowBase
{
public:
	WindowBase() :
		windowHandle(nullptr)
	{
		//w_width()
	}

	~WindowBase()
	{

	}

protected:
	WindowHandle	windowHandle; // Simple wrapper around a window handle
	String			title;

	//R_Object(Renderer)*	renderer;

	Cvar w_width;
	Cvar w_height;
	Cvar w_style;
};
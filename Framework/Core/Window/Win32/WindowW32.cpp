#pragma once
#include "Core\Window\Win32\WindowW32.h"
#include "Core\Utility\Console.h"
#include "Core\Math\Vector2.h"

WindowW32::WindowW32() :
	hinstance	(nullptr),
	hdc			(nullptr)
{

}

WindowW32::~WindowW32()
{
	Destroy();
}

//----------------------------------------------------------------

bool WindowW32::Create(const String& title, u32 width, u32 height, Style style)
{
	this->title			= title;
	char*		tcstr	= const_cast<char*>(title.GetDataPtr());
	WNDCLASS	wc		= { 0 };
	hinstance			= GetModuleHandle(nullptr);		
	wc.lpfnWndProc		= WndProc;
	wc.hInstance		= hinstance;
	wc.lpszClassName	= title.GetDataPtr();
	wc.style			= CS_OWNDC | CS_HREDRAW | CS_VREDRAW;

	if (RegisterClass(&wc) == false)
		return false;

	// Need to compensate for the window border if it exists
	// otherwise the inner size will NOT be correct!
	u32 scaledWidth		= width;
	u32 scaledHeight	= height;
	RECT rect = { 0, 0, width, height };

	if (style == Style::Normal || style == Style::FixedSize)
	{
		AdjustWindowRect(&rect, static_cast<DWORD>(style), false);
		scaledWidth		= (rect.right - rect.left);
		scaledHeight	= (rect.bottom - rect.top);	
	}

	// Just try and clamp the window if it's someplace dumb for whatever reason
	s32 centeredX = (GetSystemMetrics(SM_CXSCREEN) - scaledWidth) / 2; 
	s32 centeredY = (GetSystemMetrics(SM_CYSCREEN) - scaledHeight) / 2; 
	if (centeredX > GetSystemMetrics(SM_CXSCREEN)) centeredX = 0;
	if (centeredY > GetSystemMetrics(SM_CYSCREEN)) centeredY = 0;
	
	windowHandle = CreateWindowEx(0, tcstr, tcstr, static_cast<DWORD>(style), centeredX, centeredY, scaledWidth, scaledHeight, 0, 0, 0, 0);

	if (windowHandle)
	{
		// We should be ok at this point!
		ShowWindow(windowHandle, SW_SHOW);
		return true;
	}

	return false;
}

bool WindowW32::Destroy()
{
	u32 ret = UnregisterClass(title.GetDataPtr(), hinstance);

	if (ret > 0)
		return true;

	return false;
}

//----------------------------------------------------------------

bool WindowW32::GetEvents(Event& event) const
{
	while (PeekMessage(&event, 0, 0, 0, PM_REMOVE))
	{
		DispatchMessage(&event);
	}

	return (event.message == WM_QUIT) ? false : true;
}

bool WindowW32::GetEventsWait(Event& event) const
{
	while (GetMessage(&event, 0, 0, 0) > 0)
	{
		DispatchMessage(&event);
	}

	return (event.message == WM_QUIT) ? false : true;
}

//----------------------------------------------------------------

void WindowW32::GetCursorPosition(Vector2<u32>& point) const
{
	POINT pos = { 0 };
	GetCursorPos(&pos);
	ScreenToClient(windowHandle, &pos);

	point.X = pos.x;
	point.Y = pos.y;
};

//----------------------------------------------------------------

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}

	return 0;
}
#pragma once
#include "Core\Window\WindowBase.h"
#include "Core\Math\Vector2.h"
#include <Windows.h>
typedef MSG Event;

class BNR_API WindowW32 : public WindowBase
{
public:
	enum class Style : unsigned int
	{
		Normal		= WS_OVERLAPPEDWINDOW,					// Typical window -- min / max / close buttons, and resizable
		FixedSize	= WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME,	// Same as normal except resizing is disabled
		Borderless	= WS_BORDER | WS_POPUP,					// Does what it says on the tin
		FullScreen	= WS_BORDER | WS_POPUP | WS_EX_TOPMOST	// Always top-most window
	};

//----------------------------------------------------------------

public:
	WindowW32 ();
	~WindowW32();

//----------------------------------------------------------------

	bool Create			(const String& title, u32 width, u32 height, Style style);
	bool Destroy		();	

	bool GetEvents		(Event& event) const;
	bool GetEventsWait	(Event& event) const;

	void GetCursorPosition(Vector2<u32>& point) const;

	inline WindowHandle	GetWindowHandle () const { return windowHandle;		}
	inline u32			GetWidth		() const { return w_width.Get();	}
	inline u32			GetHeight		() const { return w_height.Get();	}

//----------------------------------------------------------------

private:
	HINSTANCE		hinstance;
	HDC				hdc;

	friend LRESULT CALLBACK	WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
};
#pragma once
#include "Core\Memory\Alloc.h"

// Why is this a thing
#if defined(small)
#undef small
#endif

// Utterly braindead dynamic managed array
template <typename T>
struct Array
{
	Array() :
		data			(nullptr),
		size			(0),
		capacity		(0),
		capacityInBytes	(0)
	{
		
	}

	Array(u32 count) :
		data			(nullptr),
		size			(0),
		capacity		(0),
		capacityInBytes	(0)
	{
		Reserve(count);
	}

	Array(const Array& array) :
		data			(nullptr),
		size			(0),
		capacity		(0),
		capacityInBytes	(0)
	{
		Reserve(array.size);
		memcpy(data, array.data, array.size * sizeof(T));
	}

	~Array()
	{
		delete data;
	}

//----------------------------------------------------------------

	Array& operator = (const Array& array)
	{
		Reserve(array.capacity);
		memcpy(data, array.data, array.capacity);

		return *this;
	}

	T& operator[](u32 index)
	{
		return data[index];
	}

	const T& operator[](u32 index) const
	{
		return data[index];
	}

//----------------------------------------------------------------

	// Simple crap
	inline bool IsValid		() const { return data != nullptr;	}
	inline bool IsEmpty		() const { return size == 0;		}
	inline u32	GetSize		() const { return size;				}
	inline u32	GetCapacity	() const { return capacity;			}
	inline T*	GetDataPtr	() const { return data;				}

	// Allocation
	inline void Reserve(u32 count)
	{
		if (count > capacity)
		{
			// Just double the object storage unless count is going to be even larger
			capacity		= (capacity * 2) > count ? capacity * 2 : count;
			capacityInBytes = capacity * sizeof(T);

			data			= static_cast<T*>(bnr_realloc(data, capacityInBytes));
		}
	}

	inline void Grow()
	{
		Reserve(capacity + 1);
	}

	// Actual array'y stuff
	inline void PushBack()
	{
		Grow();
		size++;
	}

	inline void Add(const T& value)
	{
		if ((size + 1) > capacity)
			Grow();

		data[size] = value;
		size++;
	}

	inline void Insert(u32 index, const T& value)
	{
		if (index > capacity)
			Grow();

		// We have no idea how big the array actually is in elements
		// because we can't know what IS a valid element... so just
		// assume the size to be the index if larger
		if ((index + 1) > size)
			size = index + 1;

		data[index] = value;
	}

	// Other stuff
	inline void Clear()
	{
		memset(data, 0, capacityInBytes);
		size			= 0;
		capacity		= 0;
		capacityInBytes = 0;
	}

//----------------------------------------------------------------

protected:
	T*		data;			// The obvious
	u32		size;			// Number of objects in the array
	u32		capacity;		// Actual capacity of the array, IN OBJECTS

	u32		capacityInBytes;
};
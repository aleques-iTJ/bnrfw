#pragma once
#include "Core\Memory\Alloc.h"

#if defined(small)
#undef small
#endif

// Fixed size array, if it wasn't obvious enough
template <typename T, u32 capacity>
struct FixedArray
{
	FixedArray() :
		size(0)
	{

	}

//----------------------------------------------------------------

	T& operator[](u32 index)
	{
		return data[index];
	}

	const T& operator[](u32 index) const
	{
		return data[index];
	}

//----------------------------------------------------------------

	inline bool IsEmpty		() const { return size == 0;		}
	inline u32	GetSize		() const { return size;				}
	inline u32	GetCapacity	() const { return capacity;			}

	inline T* const GetDataPtr() const
	{
		return data;
	}

	inline T* GetDataPtr()
	{
		return data;
	}

//----------------------------------------------------------------

	inline void Add(const T& value)
	{
		data[size++] = value;
	}

	inline void Insert(u32 index, const T& value)
	{
		if ((index + 1) > size)
			size = index + 1;

		data[index] = value;
	}

	inline void Clear()
	{
		memset(data, 0, sizeof(T) * Size);
	}

//----------------------------------------------------------------

protected:
	T	data[capacity];
	u32 size;
};
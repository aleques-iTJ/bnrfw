#pragma once
#include "Core\Containers\Array.h"

// Utterly braindead dynamic managed string
// This thing is kinda sketchy...
struct String : public Array<char>
{
	String()
	{
		// Make sure we explicitly set capacity to the size of our small string
		capacity = 64;
	}

	String(u32 length) :
		Array(length)
	{
		
	}

	// Copying and moving, the String overloads use Set(char*) because it
	// will actually strlen what's coming in
	String(const char* string)
	{
		capacity = 64;
		Set(string);
	}

	String(const String& string)
	{
		capacity = 64;
		Set(string.GetDataPtr());
	}

	~String()
	{
		if (data != nullptr) // Make sure we're actually pointing to heap memory
		{
			delete data;
			data = nullptr;
		}
	}

//----------------------------------------------------------------

	String& operator = (const char* string)
	{
		Set(string);
		return *this;
	}

	String& operator = (const String& string)
	{
		Set(string.GetDataPtr());
		return *this;
	}

	String& operator + (const char* string)
	{
		Append(string);
		return *this;
	}

	String& operator + (const String& string)
	{
		Append(string.GetDataPtr());
		return *this;
	}

//----------------------------------------------------------------

	// Getters n stuff
	inline char*		GetDataPtr()		{ return (data == nullptr) ? small : data; }
	inline const char*	GetDataPtr() const	{ return (data == nullptr) ? small : data; }

	// Allocation
	inline void Reserve(u32 size)
	{
		// We've allocated the large string at this point
		if (data != nullptr)
		{
			capacity		= (capacity * 2) > size ? capacity * 2 : size;
			capacityInBytes = capacity;// *sizeof(char); // Is there any machine where char isn't 1 byte?

			data			= static_cast<char*>(bnr_realloc(data, capacityInBytes));
		}

		// Otherwise, check if we've exceeded the small string size
		else if (size > capacity)
		{		
			capacity		= (capacity * 2) > size ? capacity * 2 : size;
			capacityInBytes = capacity; // *sizeof(char);

			char* newData	= static_cast<char*>(bnr_realloc(nullptr, capacityInBytes));

			// Copy the old string over before we set the pointer and it's lost to the aether
			size_t smallEnd = strlen(small);
			memcpy(newData, small, smallEnd);
			data = newData;

			// Just null terminate after the size...
			// The right thing to do might just be to zero the entire buffer before hand
			data[smallEnd] = 0;
		}		
	}

	// String'y stuff
	inline void Set(const char* string)
	{
		// Need to account for the zero termination!
		size = static_cast<u32>(strlen(string));

		Reserve(size);
		strcpy(GetDataPtr(), string);
	}

	inline void Set(const String& string)
	{
		Reserve(string.GetSize());
		strcpy(GetDataPtr(), string.GetDataPtr());
	}

	inline void Append(const char* string)
	{
		size = size + static_cast<u32>(strlen(string));

		Reserve(size);
		strcat(GetDataPtr(), string);
	}

	inline void Append(const String& string)
	{
		if (&string != this)
			Append(string.GetDataPtr());

		else
		{
			String result = *this;
			result.Append(string.GetDataPtr());
			*this = result;
		}
	}

//----------------------------------------------------------------

private:
	char small[64];	// Don't allocate for tiny strings, can probably get away with this in most cases
};
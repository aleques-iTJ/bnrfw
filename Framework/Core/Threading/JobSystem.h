#pragma once
#include "Core\Containers\Array.h"

typedef delegate<void*()> Job;

// A JobList is similar to a queue, which subsystems will effectively manage on their own
// Because they're independent like that, it will only require syncronization to actually
// submit the final JobList to the JobManager, which hands off work to the worker threads
struct JobList
{
public:
	void Add	(Job job) { jobs.Add(job); }
	void Submit	();
	void Wait	();

private:
	Array<Job> jobs;
};

struct JobManager
{

private:
	Array<JobList>		jobLists;
	Array<ThreadW32>	threads;

	u32 numThreads;
	u32 numJobs;
};

extern JobManager* g_JobManager;
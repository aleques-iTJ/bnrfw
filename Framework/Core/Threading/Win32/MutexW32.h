#pragma once
#include "Core\Types.h"

// Simple mutex wrapper
struct MutexW32
{
public:
	MutexW32() :
		handle(nullptr)
	{
		InitializeCriticalSection(handle);
	}

	~MutexW32()
	{
		DeleteCriticalSection(handle);
	}

//----------------------------------------------------------------

	bool Lock(bool blocking = true)
	{
		if (TryEnterCriticalSection(handle) == 0)
		{
			// Something else owns the critical section right now
			// Just bail if we don't want to block
			if (blocking == false)
				return false;

			EnterCriticalSection(handle);
		}

		return true;
	}

	void Unlock()
	{
		LeaveCriticalSection(handle);
	}

//----------------------------------------------------------------

private:
	CRITICAL_SECTION* handle;
};
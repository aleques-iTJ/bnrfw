#pragma once
#include "Core\Types.h"
#include "Extern\TinyCThread\tinycthread.h"

struct SignalW32
{
public:
	SignalW32() :
		handle(nullptr)
	{ 
		CreateEvent(nullptr, false , false, nullptr);
	}

	~SignalW32()
	{ 
		CloseHandle(handle);
	}

//----------------------------------------------------------------

	bool Raise(){ return SetEvent(handle)						!= 0;	}
	bool Clear(){ return ResetEvent(handle)						!= 0;	}
	bool Wait (){ return WaitForSingleObject(handle, INFINITE)	!= 0;	}

//----------------------------------------------------------------

private:
	HANDLE handle;
};
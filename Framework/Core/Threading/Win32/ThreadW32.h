#pragma once
#include "Core\Types.h"

struct ThreadW32
{
public:
	ThreadW32() :
		handle		(nullptr),
		isBusy		(false),
		isExiting	(false),
		isWorker	(false)
	{
		
	}

	~ThreadW32()
	{
		CloseHandle(handle);
	}

	
	bool Create(bool asWorker)
	{
		// If a thread is created as a worker, it's immediately put to sleep until signaled
		CreateThread(nullptr, 0, )

			NULL,              // default security
			0,                 // default stack size
			ThreadProc,        // name of the thread function
			NULL,              // no thread parameters
			0,                 // default startup flags
			&dwThreadID);

	}

	//bool Lock(bool blocking = true)	{ return mtx_lock(handle); }
	//bool Unlock()						{ return mtx_unlock(handle); }

private:
	HANDLE		handle;
	MutexW32	mutex;
	SignalW32	signal;
	

	bool	isBusy;
	bool	isExiting;
	bool	isWorker;
};
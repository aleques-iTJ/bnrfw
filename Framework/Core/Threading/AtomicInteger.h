#pragma once
#include "Core\Types.h"

struct AtomicInteger
{
public:
	AtomicInteger() :
		integer(0)
	{
		
	}

	~AtomicInteger()
	{
		
	}

//----------------------------------------------------------------

	s32 Increment()
	{
		return InterlockedIncrement(&integer);
	}

	s32 Decrement()
	{
		return InterlockedDecrement(&integer);
	}

	s32 Add(LONG value)
	{
		// I have no idea what is going on
		value = 0;
	}

	s32 Subtract(LONG value)
	{
		// Send help I need an adult
		value = 0;
	}

	s32 GetValue() const
	{
		return integer;
	};

//----------------------------------------------------------------

private:
	LONG integer;
};
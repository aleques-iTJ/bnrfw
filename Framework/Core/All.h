#pragma once

// Containers
#include "Core\Containers\Array.h"
#include "Core\Containers\String.h"

// FileSystem
#include "Core\FileSystem\File.h"
#include "Core\FileSystem\FileSystem.h"

// Math
#include "Core\Math\Vector2.h"

// Memory / allocators
#include "Core\Memory\Alloc.h"
#include "Core\Memory\Memory.h"

// System

// Threading
#include "Core\Threading\AtomicInteger.h"
#include "Core\Threading\JobSystem.h"


// Other
#include "Core\Utility\Console.h"
#include "Core\Utility\Cvar.h"
#include "Core\Types.h"

// Platform specific
#if defined (_WIN32)
	// System
	#include "Core\System\SystemW32.h"

	// Threading
	#include "Core\Threading\Win32\MutexW32.h"
	#include "Core\Threading\Win32\SignalW32.h"
	#include "Core\Threading\Win32\ThreadW32.h"

	// Utility
	#include "Core\Utility\Win32\TimerW32.h"

	// Window
	#include "Core\Window\Win32\WindowW32.h"


	typedef TimerW32	Timer;
	typedef WindowW32	Window;
	typedef MutexW32	Mutex;
	typedef SignalW32	Signal;
	typedef ThreadW32	Thread;
#endif
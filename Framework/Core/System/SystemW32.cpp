#pragma once
#include "Core\System\SystemW32.h"

String System::GetExeDirectory()
{
	char exePath	 [MAX_PATH];
	char exeDirectory[MAX_PATH];

	GetModuleFileName(nullptr, exePath, MAX_PATH);

	size_t i = strlen(exePath);
	while (exePath[i] != '\\')
		i--;

	strncpy(exeDirectory, exePath, i);
	exeDirectory[i + 0] = '\\';
	exeDirectory[i + 1] = '\0';

	return String(exeDirectory);
}
#pragma once
#include "Core\Utility\TimerBase.h"

TimerBase::TimerBase() :
	current			(0),
	last			(0),
	ms				(0.0),
	accumulator		(0.0)
{

}

TimerBase::~TimerBase()
{
	/*nada*/
}

//----------------------------------------------------------------

f64 TimerBase::GetHz()
{
	return 1000.0 / ms;
}

f64 TimerBase::GetMs()
{
	return ms;
}

f64 TimerBase::GetSeconds()
{
	return ms / 1000.0;
}

f64 TimerBase::GetAccumulator()
{
	return accumulator;
}

void TimerBase::ResetAccumuator()
{
	accumulator = 0.0;
}
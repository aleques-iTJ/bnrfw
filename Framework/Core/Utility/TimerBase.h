#pragma once
#include "Core\Types.h"

class BNR_API TimerBase
{
public:
	TimerBase	();
	~TimerBase	();

	// These will return the delta time in the corresponding format
	f64 GetHz();
	f64 GetMs();
	f64 GetSeconds();

	// Grab / reset the current accumulator, increments in milliseconds
	f64 GetAccumulator	();
	void ResetAccumuator();

protected:
	u64 current;
	u64 last;
	f64 ms;
	f64 accumulator;
};
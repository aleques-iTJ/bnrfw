#pragma once
#include "Core\Types.h"
#include "Core\Containers\Array.h"

// This should probably be rethought a bit so it's more friendly for other platforms
struct VideoMode
{
	VideoMode();
	VideoMode(u32 width, u32 height, u32 refresh = 0, bool fullscreen = false, bool vsync = false);

//----------------------------------------------------------------

	static Array<VideoMode>		GetAvailableModes();
	static VideoMode			GetCurrentMode();

	u32		Width;
	u32		Height;
	u32		Refresh;
	bool	Fullscreen;
	bool	VSync;
};
#pragma once
#include "Core\Types.h"
#include "Core\Containers\String.h"

typedef delegate<void()> CvarFunc;

// The Cvar class is effectively a description of a Cvar before it's added to the
// Cvar registry. Once it's registered, the object added to the registry is 
// effectively immutable and needs to be re-added for any change to take place
struct Cvar
{
	friend struct CvarReturnProxy;
	friend class  Console;

	// Simple internal type identifier
	enum class Type
	{
		Nothing		= 0,		// Unused
		Int			= 1 << 0,
		Float		= 1 << 1,
		String		= 1 << 2,
		Func		= 1 << 3
	};

	struct CvarValue
	{
		// Just convert it to an int
		CvarValue& operator = (bool value)
		{
			_int	= value;
			type	= Cvar::Type::Int;
			return *this;
		}

		CvarValue& operator = (u32 value)
		{
			_int	= value;
			type	= Cvar::Type::Int;
			return *this;
		}

		CvarValue& operator = (f32 value)
		{
			_float	= value;
			type	= Cvar::Type::Float;
			return *this;
		}

		CvarValue& operator = (String value)
		{
			_string	= value;
			type	= Cvar::Type::String;
			return *this;
		}

		CvarValue& operator = (CvarFunc value)
		{
			_func = value;
			type = Cvar::Type::Func;
			return *this;
		}

		// Proxy return
		operator bool() const
		{
			return _int != 0; // Just treat bool as an int...
		}

		operator u32() const
		{
			return _int;
		}

		operator f32() const
		{
			return _float;
		}

		operator String() const
		{
			return _string;
		}

	private:
		u32			_int;
		f32			_float;
		String		_string;
		CvarFunc	_func;

		Type		type;
	};

	// Just a simple description of it
	String		Name;
	CvarValue	Value;

	Cvar()	{ };
	~Cvar() { };

	template<typename T>
	Cvar(const String& name, T value)
	{
		Create(name, value);
	};

	template<typename T>
	void Create(const String& name, T value)
	{
		Name	= name;
		Value	= value;
	}

	// If you have access to the Cvar object, you can do a lightweight value
	// lookup instead of having to go through the Cvar map lookup
	CvarValue Get() const
	{
		return Value;
	}
};
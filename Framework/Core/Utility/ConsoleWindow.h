#pragma once
#include "bnrConfig.h"

// Window management is already abstracted
#if defined (BNR_PLATFORM_WIN32)
	#include "Core\Window\Win32\WindowW32.h"
	typedef WindowW32 Window;
#endif

class ConsoleWindow
{
public:
	ConsoleWindow ();
	~ConsoleWindow();

//----------------------------------------------------------------

	bool Create()
	{
		window = new Window();
		window->Create("bnrConsole", 320, 240, Window::Style::Normal);
	};

	bool Destroy();

	private:
		Window* window;
};
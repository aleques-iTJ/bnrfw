#pragma once

template<typename T>
struct RefCounted
{
public:
	RefCounted() :
		internalPointer (nullptr),
		refCount		(1)
	{

	}

	virtual ~RefCounted()
	{
		if (internalPointer)
			Release();
	}

//----------------------------------------------------------------

	inline u32 AddRef()
	{
		return refCount++;
	}

	inline u32 Release()
	{
		if (--refCount == 0)
		{
			delete this;
			internalPointer = nullptr;
		}
	}

	RefCounted(T* ptr) :
		internalPointer(ptr)
	{
		if (internalPointer) // Make sure something is assigned
		{
			internalPointer->AddRef();
		}
	}

	operator T*() const
	{
		return internalPointer;
	}

	T* GetUnderlying() const
	{
		return internalPointer;
	}

	T& operator*() const
	{
		return *internalPointer;
	}

	T** operator&()
	{
		return &internalPointer;
	}

	T* operator->() const
	{
		return internalPointer;
	}

protected:
	T* internalPointer;

private:
	u32 refCount;
};
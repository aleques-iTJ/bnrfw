#pragma once
#include "Core\Utility\Win32\TimerW32.h"

TimerW32::TimerW32() :
	freq(0)
{
	Create();
}

TimerW32::~TimerW32()
{
	/*nada*/
}

//----------------------------------------------------------------

void TimerW32::Create()
{
	QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&freq));
	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&current));

};

void TimerW32::Update()
{
	last = current;
	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&current));

	ms			= ((current - last) * 1000.0) / freq;
	accumulator += ms;
}

u64 TimerW32::Now()
{
	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&current));
	return current;
}

u64 TimerW32::Last()
{
	return last;
}
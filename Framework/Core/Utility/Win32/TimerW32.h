#pragma once
#include "Core\Utility\TimerBase.h"

class BNR_API TimerW32 : public TimerBase
{
public:
	TimerW32	();
	~TimerW32	();

	// Create must be called to init the high resolution timer!
	void Create();

	// Tick the timer - refreshes the data, gets the delta time, and grows the accumulator
	void Update();

	// The obvious, the current and last ticks
	u64 Now	();
	u64 Last();

private:
	u64 freq;
};
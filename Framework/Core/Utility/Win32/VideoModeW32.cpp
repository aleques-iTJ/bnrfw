#pragma once
#include "Core\Utility\VideoMode.h"
#include <Windows.h> 

VideoMode::VideoMode() :
	Width		(640),
	Height		(400),
	Refresh		(60),
	Fullscreen	(false),
	VSync		(false)
{

}

VideoMode::VideoMode(u32 width, u32 height, u32 refresh /*= 0*/, bool fullscreen /*= false*/, bool vsync /*= false*/) :
	Width		(width),
	Height		(height),
	Refresh		(refresh),
	Fullscreen	(fullscreen),
	VSync		(vsync)
{

}

//----------------------------------------------------------------

Array<VideoMode> VideoMode::GetAvailableModes()
{

	DEVMODE				mode;
	u32					count = 0;
	Array<VideoMode>	modes;

	mode.dmSize = sizeof(DEVMODE);
	while (EnumDisplaySettings(0, count, &mode))
	{
		// Refresh here is returned as a whole number...
		// this may not be "correct" behavior for D3D!
		modes.Add(VideoMode(mode.dmPelsWidth,
							mode.dmPelsHeight,
							mode.dmDisplayFrequency,
							false,
							false));
		count++;
	}

	return modes;

}

VideoMode VideoMode::GetCurrentMode()
{
	DEVMODE		mode;
	VideoMode	modes;

	mode.dmSize = sizeof(DEVMODE);
	EnumDisplaySettings(0, ENUM_CURRENT_SETTINGS, &mode);

	modes.Width			= mode.dmPelsWidth;
	modes.Height		= mode.dmPelsHeight;
	modes.Refresh		= mode.dmDisplayFrequency;
	modes.Fullscreen	= false;
	modes.VSync			= false;

	return modes;
}
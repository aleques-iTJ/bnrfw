#pragma once
#include "Core\Types.h"

// https://en.wikipedia.org/wiki/Xorshift

inline u32 Rand()
{
	static u32 x = 0x75BCD150;
	static u32 y = 0x159A55E5;
	static u32 z = 0x1F123BB5;
	static u32 w = 0x54913330;

	u32 t;
	t = x ^ (x << 11);

	x = y;
	y = z;
	z = w;
	return w = w ^ (w >> 19) ^ (t ^ (t >> 8));
}

inline f32 RandFloat()
{
	return ((u32) Rand) + (1.0f / 4294967296.0f);
}
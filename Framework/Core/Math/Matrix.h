#pragma once
#include <DirectXMath.h>
#include "Core\Types.h"

using namespace DirectX;

struct Vector;
struct Matrix : public XMMATRIX
{
	Matrix() : XMMATRIX()
	{

	}

	Matrix(const XMMATRIX& matrix) : XMMATRIX(matrix)
	{

	}

	Matrix(const Matrix& matrix) : XMMATRIX(matrix)
	{

	}

	//----------------------------------------------------------------

	operator XMMATRIX() const
	{
		return *this;
	}

	//----------------------------------------------------------------

	Matrix& operator = (XMMATRIX& matrix)
	{
		memcpy(this, &matrix, sizeof(Matrix));
		return *this;
	}

	Matrix& operator = (Matrix& matrix)
	{
		memcpy(this, &matrix, sizeof(Matrix));
		return *this;
	}

	//----------------------------------------------------------------

	static inline bool IsIdentity(const Matrix& matrix)
	{
		return XMMatrixIsIdentity(matrix);
	}

	static inline Matrix Indentity()
	{
		return static_cast<Matrix>(XMMatrixIdentity());
	}

	static inline Matrix Perspective(f32 fovy, f32 aspect, f32 nearZ, f32 farZ)
	{
		return static_cast<Matrix>(XMMatrixPerspectiveFovLH(fovy, aspect, nearZ, farZ));
	}

	static inline Matrix Ortho2D(f32 width, f32 height, f32 zn, f32 zf)
	{
		return static_cast<Matrix>(XMMatrixOrthographicOffCenterLH(0, width, height, 0, zn, zf));
	}

	/*static inline Matrix LookAt(const Vector& eye, const Vector& at, const Vector& up)
	{
		//return static_cast<Matrix>(XMMatrixLookAtLH(eye, at, up));
	}*/

	static inline Matrix Multiply(const Matrix& m1, const Matrix& m2)
	{
		return static_cast<Matrix>(XMMatrixMultiply(m1, m2));
	}

	static inline Matrix Scale(f32 x, f32 y, f32 z)
	{
		return static_cast<Matrix>(XMMatrixScaling(x, y, z));
	}

	static inline Matrix Translate(f32 x, f32 y, f32 z)
	{
		return static_cast<Matrix>(XMMatrixTranslation(x, y, z));
	}

	static inline Matrix RotateX(f32 angle)
	{
		return static_cast<Matrix>(XMMatrixRotationX(angle));
	}

	static inline Matrix RotateY(f32 angle)
	{
		return static_cast<Matrix>(XMMatrixRotationY(angle));
	}

	static inline Matrix RotateZ(f32 angle)
	{
		return static_cast<Matrix>(XMMatrixRotationZ(angle));
	}
};
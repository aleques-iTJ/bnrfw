#pragma once
#include <DirectXMath.h>
#include "Core\Types.h"

using namespace DirectX;

struct Vector : public XMFLOAT4A
{
	Vector(f32 x, f32 y, f32 z, f32 w)
	{
		*this = XMVectorSet(x, y, z, w);
	}

	Vector(const XMFLOAT4A& vector) : XMFLOAT4A(vector)
	{

	}

	Vector(const Vector& vector) : XMFLOAT4A(vector)
	{

	}

	//----------------------------------------------------------------

	operator XMVECTOR() const
	{
		return XMLoadFloat4A(this);
	}

	//----------------------------------------------------------------

	Vector& operator = (const XMVECTOR& vector)
	{
		XMStoreFloat4A(this, vector);
		return *this;
	}

	Vector& operator = (const Vector& vector)
	{
		*this = XMFLOAT4A(vector);
		return *this;
	}
};
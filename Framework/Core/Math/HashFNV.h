#pragma once
#include "Core\Types.h"

u32 Hash(const char* path)
{
	const u32 Prime			= 16777619;
	const u32 OffsetBasis	= 2166136261;

	u32 hash = OffsetBasis;

	u32 len = strlen(path);
	for (u32 i = 0; i < len; i++)
	{
		hash = hash ^ path[i];
		hash = hash * Prime;
	}

	return hash;
}
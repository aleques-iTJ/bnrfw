#pragma once
#include <cmath>
#include "Core\Types.h"

const f32 Pi	= 3.141592653f;
const f32 PiRcp = 1.0f / Pi;

//----------------------------------------------------------------

inline f32 ToRadians(f32 x)
{
	return x * (Pi / 180);
}

inline f32 ToDegrees(f32 x)
{
	return x * (180 / Pi);
}

inline f32 ToVertFov(f32 x, f32 aspect)
{
	return 2 * static_cast<f32>(atan(tan(x * 0.5) / aspect));
}

inline f32 ToHorFov(f32 x, f32 aspect)
{
	return 2 * static_cast<f32>(atan(tan(x * 0.5) * aspect));
}

inline void SinCos(f32 x, f32& s, f32& c)
{
	s = sin(x);
	c = cos(x);

	/*__asm
	{
		mov eax, dword ptr [c]
		mov edx, dword ptr [s]
		fld		 dword ptr [x]
		fsincos
		fstp	 dword ptr [eax] // Cos first
		fstp	 dword ptr [edx] // Sin last
	}*/
}

inline f32 Cot(f32 x)
{
	f32 s, c;
	SinCos(x, s, c);
	return c / s;
}

inline s32 Round(f32 x)	// AKA a fast float to int
{
	return _mm_cvtss_si32(_mm_load_ss(&x));
}

inline f32 Clamp(f32 x, f32 min, f32 max)
{
	return (x < min) ? min : ((x > max) ? max : x);
}

inline f32 Saturate(f32 x)
{
	return Clamp(x, 0.0f, 1.0f);
}
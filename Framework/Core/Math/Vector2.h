#pragma once
#include "Core\Types.h"

template<typename T>
struct Vector2
{
	T X;
	T Y;
};
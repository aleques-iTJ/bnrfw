#pragma once
#include <math.h>

template<typename T>
struct Complex
{
	T Real;
	T Imag;

	Complex(T real, T imaginary)
	{
		Real = real;
		Imag = imaginary;
	}

	Complex operator + (const Complex& complex)
	{
		T real, imag;
		real = (Real + complex.Real); // (a + c) +
		imag = (Imag + complex.Imag); // (b + d)
		return Complex(real, imag);
	}

	Complex operator * (const Complex& complex)
	{
		T real, imag;
		real = (Real * complex.Real) - (Imag * complex.Imag); // (ac - bd) +
		imag = (Imag * complex.Real) + (Real * complex.Imag); // (bc + ad)
		return Complex(real, imag);
	}

	T Absolute() const
	{
		return sqrt((Real * Real) + (Imag * Imag));
	}
};
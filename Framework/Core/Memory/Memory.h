#pragma once
#include "Core\Types.h"
#include <emmintrin.h>
#include <intrin.h>

namespace Memory
{
	// I tried a larger 256 bit one under 64 bit, but it's slower
	// A register pressure thing, maybe? I dunno.

	inline void* Copy(void* destination, const void* source, size_t num)
	{
		// holy gross
		char* src = const_cast<char*>(reinterpret_cast<const char*>(source));
		char* dst = reinterpret_cast<char*>(destination);

		u32 i = 0;

		// The main SSE block for moving large chunks
		for (; i < num; i += 128)
		{
			// [i + (0 << 4)] indexes on 16 byte spans
			// Reads
			__m128i xmm0 = _mm_load_si128(reinterpret_cast<__m128i*>(&src[i + (0 << 4)]));
			__m128i xmm1 = _mm_load_si128(reinterpret_cast<__m128i*>(&src[i + (1 << 4)]));
			__m128i xmm2 = _mm_load_si128(reinterpret_cast<__m128i*>(&src[i + (2 << 4)]));
			__m128i xmm3 = _mm_load_si128(reinterpret_cast<__m128i*>(&src[i + (3 << 4)]));
			__m128i xmm4 = _mm_load_si128(reinterpret_cast<__m128i*>(&src[i + (4 << 4)]));
			__m128i xmm5 = _mm_load_si128(reinterpret_cast<__m128i*>(&src[i + (5 << 4)]));
			__m128i xmm6 = _mm_load_si128(reinterpret_cast<__m128i*>(&src[i + (6 << 4)]));
			__m128i xmm7 = _mm_load_si128(reinterpret_cast<__m128i*>(&src[i + (7 << 4)]));

			// Writes, non temporal
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (0 << 4)]), xmm0);
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (1 << 4)]), xmm1);
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (2 << 4)]), xmm2);
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (3 << 4)]), xmm3);
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (4 << 4)]), xmm4);
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (5 << 4)]), xmm5);
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (6 << 4)]), xmm6);
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (7 << 4)]), xmm7);			
		}

		for (; i + 16 < num; i += 16)
		{
			// dqword about it
			__m128i dqword = _mm_load_si128(reinterpret_cast<__m128i*>(&src[i]));
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i]), dqword);
		}

		for (; i + 4 < num; i += 4)
			*reinterpret_cast<u32*>(&dst[i]) = *reinterpret_cast<u32*>(&src[i]);

		for (; i < num; i++)
			dst[i] = src[i];

		return dst;
	}

	inline void* Set(void* ptr, int value, size_t num)
	{
		__m128i temp	= _mm_set1_epi32(value);
		__m128i val		= _mm_load_si128(&temp);
		char*	dst		= reinterpret_cast<char*>(ptr);

		size_t i = 0;
		for (; i <= num; i += 128)
		{
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (0 << 4)]), val);
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (1 << 4)]), val);
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (2 << 4)]), val);
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (3 << 4)]), val);
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (4 << 4)]), val);
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (5 << 4)]), val);
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (6 << 4)]), val);
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i + (7 << 4)]), val);
		}

		for (; i + 16 <= num; i += 16)
		{
			_mm_stream_si128(reinterpret_cast<__m128i*>(&dst[i]), val);
		}

		for (; i + 4 <= num; i += 4)
		{
			*reinterpret_cast<u32*>(&dst[i]) = *reinterpret_cast<u32*>(&value);
		}

		for (; i <= num; i++)
		{
			dst[i] = static_cast<char>(value);
		}

		return dst;
	}
}
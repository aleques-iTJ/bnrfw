#pragma once
#include <cstdlib>
#include "Core\Types.h"

// Simple wrapper around new / delete
inline void*	bnr_malloc	(size_t size);
inline void*	bnr_realloc	(void* ptr, size_t size);
inline void		bnr_free	(void* ptr);

//----------------------------------------------------------------

void* bnr_malloc(size_t size)
{
	void *p = _aligned_malloc(size, 16);

	#ifdef _DEBUG
		DebugPrintf("alloc\t->\t%u bytes\t\t@ %p\n", size, p);
	#endif

	return p;
}

void* bnr_realloc(void* ptr, size_t size)
{
	void *p = _aligned_realloc(ptr, size, 16);

	#ifdef _DEBUG
		DebugPrintf("realloc\t->\t%u bytes\t\t@ %p\n", size, p);
	#endif

	return p;
}

void bnr_free(void* ptr)
{
	#ifdef _DEBUG
		DebugPrintf("free\t->\t%p\n", ptr);
	#endif

	_aligned_free(ptr);

	#ifdef _DEBUG
		ptr = nullptr;
	#endif
}

#define malloc(size) bnr_malloc(size)
#define realloc(ptr, size) bnr_realloc(ptr, size)
#define free(ptr) bnr_free(ptr)

//----------------------------------------------------------------

inline void* operator new(size_t size)
{
	return bnr_malloc(size);
}

inline void* operator new[](size_t size)
{
	return bnr_malloc(size);
}

inline void operator delete(void* p)
{
	if (p != nullptr)
		bnr_free(p);
}

inline void operator delete[](void* p)
{
	if (p != nullptr)
		bnr_free(p);
}
#pragma once
#include "Core\Memory\Allocator.h"

namespace bnr
{
//----------------------------------------------------------------

class LinearAllocator : Allocator<LinearAllocator>
{
public:
	LinearAllocator	(u32 size = 0);
	~LinearAllocator();

	void Create	(u32 size = 0);
	void Destroy();

	void* operator new(size_t size)
	{
		return bnr_malloc(size);
	}

	void* operator new[](size_t size)
	{
		return bnr_malloc(size);
	}

	void operator delete(void* p)
	{
		bnr_free(p);
	}

	void operator delete[](void* p)
	{
		bnr_free(p);
	}
};

//----------------------------------------------------------------
}


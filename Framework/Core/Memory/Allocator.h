#pragma once
#include "Core\Memory\Alloc.h"

namespace bnr
{
//----------------------------------------------------------------

// Base memory allocator
template <typename T>
class Allocator
{
public:
	Allocator			(u32 size, u8 alignment);
	virtual  ~Allocator	();

	void Create	(u32 size = 0);
	void Destroy();

	void* operator new(size_t size)
	{
		return bnr_malloc(size);
	}

	void* operator new[](size_t size)
	{
		return bnr_malloc(size);
	}

	void operator delete(void* p)
	{
		bnr_free(p);
	}

	void operator delete[](void* p)
	{
		bnr_free(p);
	}

protected:
	u32 memUsed;
	u32 numAllocations;

private:
	u32 getUsedMemory()
	{
		return memUsed;
	}

	u32 getNumAllocations()
	{
		return numAllocations;
	}
};

//----------------------------------------------------------------
}

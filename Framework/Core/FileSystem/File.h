#pragma once
#include "Core\FileSystem\FileSystem.h"
#include <cstdio>

struct BNR_API File
{
	friend class FileSystem;

//----------------------------------------------------------------

	File() :
		ptr		(nullptr),
		data	(nullptr),
		fileSize(0)
	{
		// idk
	}

	~File()
	{
		delete[] data;
		
		//delete this;
	}

//----------------------------------------------------------------

	u32		GetSize()			{ return fileSize; }
	String	GetPath()			{ return filePath; }
	void*	GetDataPtr() const	{ return data;	   }

//----------------------------------------------------------------

private:
	FILE*	ptr;
	void*	data;

	u32			fileSize;
	String		filePath;
	FileAccess	fileAccess;
};
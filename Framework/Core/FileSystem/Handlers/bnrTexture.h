#pragma once
#include "Core\Types.h"

// File handler for bnrFW Texture files
struct _bnrTextureHeader
{
	// Base header

	// 10
	u8	Identifier[4];
	u8	Version;
	u8	HeaderSize;
	u32	FileSize;

	// 8, core
	u16	Width;
	u16	Height;
	u16	Depth;
	u16	Flags;

	// 3, extra
	u8	ArrayLength;
	u8	NumFrames;
	u8	FrameRate;

	void* Data;
};
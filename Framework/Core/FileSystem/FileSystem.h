#pragma once
#include "Core\Types.h"
#include "Core\Containers\String.h"

#pragma warning(disable : 4251)

struct File;
struct String;

//----------------------------------------------------------------

enum class FileAccess
{
	Read,			// This will ONLY succeed if the file exists!
	Write,			// This will CREATE a file if nothing exists!

	ReadWrite,		// This will ONLY succeed if the file exists!
	ReadWriteForced // This will CREATE (or DESTROY) a file if it already exists!
};

//----------------------------------------------------------------

class BNR_API FileSystem
{
public:
	FileSystem ();
	FileSystem (const String& basePath);
	~FileSystem();

	File*	OpenFile (const String& path, FileAccess fileAccess);
	bool	CloseFile(File* file);

	u32		ReadFile (File* file);
	u32		ReadFile (File* file, void* ptr);
	u32		WriteFile(File* file);

private:
	String basePath;
};
#pragma once
#include "Core\FileSystem\FileSystem.h"
#include "Core\FileSystem\File.h"

FileSystem::FileSystem()
{

}

FileSystem::FileSystem(const String& basePath)
{
	this->basePath = basePath;
}

FileSystem::~FileSystem()
{

}

//----------------------------------------------------------------

File* FileSystem::OpenFile(const String& path, FileAccess fileAccess)
{
	char accessStr[4] = { 0, 0, 0, 0 };
	if		(fileAccess == FileAccess::Read)			strcpy(accessStr, "r");
	else if (fileAccess == FileAccess::Write)			strcpy(accessStr, "w");
	else if (fileAccess == FileAccess::ReadWrite)		strcpy(accessStr, "r+");
	else if (fileAccess == FileAccess::ReadWriteForced)	strcpy(accessStr, "w+");

	File* file	= new File;
	file->ptr	= fopen(path.GetDataPtr(), accessStr);

	if (file->ptr == nullptr)
		return nullptr;

	// Grab the file size
	fseek(file->ptr, 0, SEEK_END);
	file->fileSize = ftell(file->ptr);
	fseek(file->ptr, 0, SEEK_SET);

	// Save some extra info
	file->filePath		= path;
	file->fileAccess	= fileAccess;

	return file;
};

bool FileSystem::CloseFile(File* file)
{
	return fclose(file->ptr) != 0;
}

u32 FileSystem::ReadFile(File* file)
{
	file->data = new u8[file->fileSize];
	return static_cast<u32>(fread(file->data, 1, static_cast<u32>(file->fileSize), file->ptr));
}

u32 FileSystem::ReadFile(File* file, void* ptr)
{
	file->data = new u8[file->fileSize];
	return static_cast<u32>(fread(ptr, 1, static_cast<u32>(file->fileSize), file->ptr));
}

u32 FileSystem::WriteFile(File* file)
{	
	return static_cast<u32>(fwrite(file->data, sizeof(char), file->fileSize, file->ptr));
}
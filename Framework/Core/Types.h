#pragma once
#pragma warning(disable : 4530)
#pragma warning(disable : 4996)
#pragma warning(disable : 4005)
//#pragma warning(disable : 4251) // dll interface
#define _CRT_SECURE_NO_WARNINGS 1
#define _HAS_EXCEPTIONS 0

#include <memory>
#include "Extern\FastDelegate\FastDelegate.h"
#include "Extern\rdestl\rdestl.h"
#include "Extern\comptr\comptr.h"
//#include "Extern\TinyCThread\tinycthread.h"

//----------------------------------------------------------------

// This is kinda wonky...
#if defined(BNR_STATIC)
	#define BNR_API // Nada
#elif defined(BNR_DYNAMIC)
	#define BNR_API __declspec(dllexport)
#elif defined(BNR_DYNAMIC_EXE)
	#define BNR_API __declspec(dllimport)

#elif !defined(BNR_API)
	#define BNR_API // Nada
#endif

//-------------------------------------------------------------------------

#ifdef _DEBUG
	#include <cstdio>
	#define DebugPrintf(str, ...) printf(str, ##__VA_ARGS__)
	#define DebugString(str) OutputDebugString(str);
#else
	#define dbgPrint(str, ...)
	#define bnr_DebugString(str)
#endif

//----------------------------------------------------------------

typedef unsigned char		u8;
typedef unsigned short		u16;
typedef unsigned int		u32;
typedef unsigned __int64	u64;
typedef char				s8;
typedef short				s16;
typedef int					s32;
typedef __int64				s64;
typedef float				f32;
typedef double				f64;

// Hurf
#define dyn_array			rde::vector
#define delegate			fastdelegate::FastDelegate
#define align16				__declspec(align(16))
#define lengthof(x)			(sizeof((x)) / sizeof((x)[0]))

//----------------------------------------------------------------

#ifdef _WIN32
	#include <Windows.h>
	typedef HWND	WindowHandle;
	typedef MSG		WindowMessage;
#elif
	typedef unsigned long* WindowHandle;
#endif
#pragma once
#include "Core\Types.h"

struct BNR_API FrameInfo
{
	u32 TrianglesDrawn;

	u32 DrawCalls;
	u32 DrawCallsIndexed;
	u32 DrawCallsInstanced;
	u32 DrawCallsIndirect;

	u32 StateChanges;
	u32 BufferBinds;
	u32 TextureSwitches;
	u32 ShaderSwitches;
};
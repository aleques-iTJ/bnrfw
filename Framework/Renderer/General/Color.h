#pragma once
#include "Core\Types.h"

// Unnamed struct warning
#pragma warning(disable : 4201)

struct BNR_API Color
{
	// TODO actually add this stuff, but not a major point
	//static Color Add		(const Color& sourceOne, const Color& sourceTwo);
	//static Color Subtract	(const Color& sourceOne, const Color& sourceTwo);

	//static Color Negative	(const Color& source);
	//static Color Lerp		()

	union
	{
		struct
		{
			f32 Alpha;
			f32 Red;
			f32 Green;
			f32 Blue;
		};

		f32		Array[4];
		//f128	Vector;
	};
};

struct ColorInt
{
	union
	{
		struct
		{
			u8 Alpha;
			u8 Red;
			u8 Green;
			u8 Blue;
		};

		u8	Array[4];
		u32 ColorARGB;
	};
};
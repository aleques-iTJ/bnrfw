#pragma once
#include "Core\Types.h"

// Does what it says on the tin, can be cast to the D3D type as is
struct BNR_API Viewport
{
	f32 TopLeftX;
	f32	TopLeftY;
	f32	Width;
	f32 Height;
	f32 MinDepth;
	f32	MaxDepth;
};
#pragma once
#include "Renderer\Backend\StateBase.h"

enum class FillMode;
enum class CullMode;

struct BNR_API RasterStateBase : public StateBase
{
public:
	FillMode	FillMode;
	CullMode	CullMode;
	u32			FrontCounterClockwise;	// bool
	u32			DepthBias;
	f32			DepthBiasClamp;
	f32			SlopeScaledDepthBias;
	u32			DepthClipEnable;		// bool
	u32			ScissorEnable;			// bool
	u32			MultisampleEnable;		// bool
	u32			AntialiasedLineEnable;	// bool
};
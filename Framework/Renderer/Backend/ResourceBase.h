#pragma once
#include "Core\Types.h"
#include "Core\Containers\Array.h"

#include "Core\Globals.h"

#include "RendererDX11\EnumsDX11.h"

struct File;

// A chunk of data, can be filled prior to creating the resource
// An immutable resource will require this to be done!
struct BNR_API Subresource
{
	Subresource(void* data, u32 stride = 0) :
		Data	(data),
		Stride	(stride)
	{

	}

	~Subresource()
	{
		// It's not the responsibility of the sub resource to manage whatever is in it!
		// delete Data;
		// Data = nullptr;
	}

//----------------------------------------------------------------

	void*	Data;
	u32		Stride;
};


class RendererBase;

enum class Format;
enum class ReadWriteAccess;
enum class BindFlags;
enum class UsageType;

struct BNR_API ResourceBase
{
public:
	ResourceBase() :
		file	(nullptr)
	{ 

	};

	~ResourceBase()
	{ 

	};

//----------------------------------------------------------------

	Array<Subresource> Subresources;

	Format			Format;
	ReadWriteAccess	RWAccess;
	BindFlags		BindAs;
	UsagePattern	Usage;
	u32				MiscFlags;

	u32				Size;
	u32				Stride;

	u32				ExtraFlags;

protected:
	// A unique ID should be created and assigned by the renderer when an object is
	// created, this will be used for sorting, indexing, etc later on. When it's no
	// longer needed, the ID should be released
	// UNUSED ATM
	u32 resourceID;

	// Size and stride when it was bound to the pipeline
	// UNUSED ATM
	u32 boundSize;
	u32 boundStride;

	// File, if the resource needs one
	File* file;

	// Some renderer specific stuff, a little gross
#include "bnrConfig.h"
#if defined (BNR_RENDERER_DX11)
	#include <d3d11.h>

	friend class RendererDX11;

	ID3D11Resource* nativeResource;
#elif
	void* nativeResource;
#endif
};
#pragma once
#include "Renderer\Backend\StateBase.h"

enum class StencilOp;
enum class CompareFunc;
enum class DepthWriteMask;

struct BNR_API DepthStencilStateBase : public StateBase
{
protected:
	struct DepthStencilOp
	{
		StencilOp	StencilFailOp;
		StencilOp	StencilDepthFailOp;
		StencilOp	StencilPassOp;
		CompareFunc StencilFunc;
	};

//----------------------------------------------------------------

public:
	u32				DepthEnable;
	DepthWriteMask	DepthWrite;
	CompareFunc		DepthFunc;
	u32				StencilEnable;
	u8				StencilReadMask;
	u8				StencilWriteMask;
	DepthStencilOp	FrontFace;
	DepthStencilOp	BackFace;
};
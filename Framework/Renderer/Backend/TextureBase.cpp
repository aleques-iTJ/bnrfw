#pragma once
#include "Renderer\Backend\TextureBase.h"
#include "Core\FileSystem\File.h"

void TextureBase::LoadFromFile(const String& filePath)
{
	File* texture		= g_FileSystem->OpenFile(filePath, FileAccess::Read);
	u32   textureSize	= g_FileSystem->ReadFile(texture);
	u8*   textureData	= static_cast<u8*>(texture->GetDataPtr());

	// Skip the first 10 bytes
	this->Width			= *(u16*)&textureData[10];
	this->Height		= *(u16*)&textureData[12];
	this->Depth			= *(u16*)&textureData[14];
	u16 flags			= *(u16*)&textureData[16];	
	u32 ddsSkew;

	this->Stride		= this->Width * 4;

	if (flags == 0)	
		this->Format = Format::RGBA;
	if (flags & 0x01)	this->Format = Format::DXT1; ddsSkew = 8;
	if (flags & 0x02)	this->Format = Format::DXT5; ddsSkew = 16;
	if (flags & 0x03)	this->Filter = FilterMode::Nearest;
	if (flags & 0x04)	this->Filter = FilterMode::Trilinear;
	if (flags & 0x05)	this->Filter = FilterMode::Anisotropic;
	if (flags & 0x06)	this->ExtraFlags = 1 << 6; // Extra flags not implemented yet
	if (flags & 0x07)	this->ExtraFlags = 1 << 7;
	if (flags & 0x08)	this->ExtraFlags = 1 << 8;

	this->NumFrames		= *(u16*)&textureData[18];
	this->FrameRate		= *(u8*)&textureData[20];
	this->MipMapCount	= *(u8*)&textureData[21];

	u8* rawData			= (u8*)&textureData[22];

	
	u32 mipOffset;
	switch (this->Format)
	{
		case Format::RGBA:
		case Format::BGRA:
			mipOffset = (this->Width * this->Height) * 4;
			break;

		case Format::DXT1:
		case Format::DXT3:
		case Format::DXT5:
			mipOffset = (max(1, ((this->Width + 3) / 4)) * max(1, ((this->Height + 3) / 4))) * ddsSkew;
			break;

		default:
			mipOffset = 0;
	}

	// Add all the mips to be bound later
	for (u32 i = 0; i < this->MipMapCount; i++)
	{
		this->Subresources.Add(Subresource(rawData, this->Stride));

		// mipOffset will be 0, but we shouldn't be here anyway if it is
		rawData		+= mipOffset;
		mipOffset	/= 4;
	}		
}
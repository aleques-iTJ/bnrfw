#pragma once
#include "Core\Types.h"
#include "Renderer\General\Viewport.h"
#include "Renderer\General\FrameInfo.h"

#include "Core\Utility\Cvar.h"

//----------------------------------------------------------------

class BNR_API RendererBase
{
	friend struct BufferBase;
	friend struct TextureBase;

	friend struct DepthStencilStateBase;
	friend struct RasterStateBase;
	friend struct SamplerStateBase;

//----------------------------------------------------------------

protected:
	WindowHandle	windowHandle;
	Viewport		viewport;
	FrameInfo		frameInfo;

	Cvar r_width;
	Cvar r_height;
	Cvar r_refresh;
	Cvar r_fullscreen;
	Cvar r_vsync;
};
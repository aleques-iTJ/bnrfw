#pragma once
#include "Renderer\Backend\StateBase.h"

enum class FilterMode;
enum class AddressMode;
enum class CompareFunc;

struct BNR_API SamplerStateBase : public StateBase
{
public:
	FilterMode	Filter;
	AddressMode	AddressU;
	AddressMode	AddressV;
	AddressMode	AddressW;
	f32			MipLODBias;
	u32			MaxAnisotropy;
	CompareFunc	ComparisonFunc;
	f32			BorderColor[4];
	f32			MinLOD;
	f32			MaxLOD;
};
#pragma once
#include "Renderer\Backend\ResourceBase.h"

enum class TextureDimension;
enum class FilterMode;
enum class AddressMode;

struct BNR_API TextureBase : public ResourceBase
{
public:
	void LoadFromFile(const String& filePath);

//----------------------------------------------------------------

	TextureDimension	Dimension;
	FilterMode			Filter;
	AddressMode			WrapMode;

	u32 Width;
	u32 Height;
	u32 Depth;

	u32 NumFrames;
	u32 FrameRate;
	u32 MipMapCount;
};
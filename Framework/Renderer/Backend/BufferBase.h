#pragma once
#include "Renderer\Backend\ResourceBase.h"

struct BNR_API BufferBase : public ResourceBase
{
public:
	// This has been pretty largely cannibalized in a refactor

	// Extra info
	u32	Offset;
};
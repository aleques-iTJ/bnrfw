#pragma once
#include "bnr.h"

#if defined (_DEBUG)
	#pragma comment(lib, "Core-Debug-x86.lib")
	#pragma comment(lib, "Renderer-Debug-x86.lib")
	#pragma comment(lib, "RendererDX11-Debug-x86.lib")

#else
	#pragma comment(lib, "Core-Release-x86.lib")
	#pragma comment(lib, "Renderer-Release-x86.lib")
	#pragma comment(lib, "RendererDX11-Release-x86.lib")

#endif
//----------------------------------------------------------------

FileSystem* g_FileSystem = new FileSystem;

class TestApp
{
public:
	TestApp();
	~TestApp();

	void Run();

private:
	Window*			window;
	Renderer*		renderer;
	SpriteBatch*	spriteBatch;

	String			basePath;
};

TestApp::TestApp()
{
	window			= new Window;
	renderer		= new Renderer;
	spriteBatch		= new SpriteBatch;

	basePath		= System::GetExeDirectory();
}

TestApp::~TestApp()
{
	// Reverse order from init
	if (spriteBatch)delete spriteBatch;
	if (renderer)	delete renderer;
	if (window)		delete window;	
}

//----------------------------------------------------------------

void TestApp::Run()
{
	const u32 width		= 1280;
	const u32 height	= 720;

	window->Create("bnr", width, height, Window::Style::Normal);	// Typical window
	renderer->Create(nullptr, width, height);
	spriteBatch->Create(renderer);

	String texPath = basePath;
	texPath.Append("testTex.bnr");
	Texture tex(true);
	tex.LoadFromFile(texPath);
	renderer->CreateTexture(tex);
	spriteBatch->SetTexture(tex);

	// Setup some sprite stuff
	u32			size	= 64;
	Rect<f32>	uv		= { 0.0f, 0.0f, 1.0f, 1.0f };
	Rect<f32>	pos		= { 0.0f, 0.0f, 0.0f, 0.0f };
	Color		col		= { 1.0f, 1.0f, 1.0f, 1.0f };

	Event ev;
	Timer timer;		
	while (window->GetEvents(ev))
	{
		timer.Update();
		f64 hz = timer.GetHz();
		f64 ms = timer.GetMs();
		f64 ac = timer.GetAccumulator();

		if (ac >= 1000.0f) // Update the framerate once per second
		{
			char str[64];
			sprintf(str, "bnrFW - FPS:%f; MS:%f", hz, ms);
			SetWindowText(window->GetWindowHandle(), str);

			timer.ResetAccumuator();
		}

//----------------------------------------------------------------

		for (u32 y = 0; y < height; y += size)
		{
			for (u32 x = 0; x < width; x += size)
			{
				// These casts...
				Rect<f32> pos = { static_cast<f32>(x), static_cast<f32>(y), static_cast<f32>(x + size), static_cast<f32>(y + size) };

				// Crap out a random color for the sprite to be modulated against
				Color color;
				color.Red	= 1.0f;
				color.Green = 1.0f;
				color.Blue	= 1.0f;
				//spriteBatch->AddQuad(pos, color, uv, 0.5f);
			}
		}


		Vector2<u32> point;
		window->GetCursorPosition(point);

		pos.X		= static_cast<u32>(point.X - (point.X / 2));
		pos.Y		= static_cast<u32>(point.Y - (point.Y / 2));
		pos.Width	= static_cast<u32>(point.X + (point.X / 2));
		pos.Height	= static_cast<u32>(point.Y + (point.Y / 2));

		f32 widthScale	= 1.0f / width;
		f32 heightScale	= 1.0f / height;
		col.Red		= ((widthScale * point.X) / 2) + ((heightScale * point.Y) / 2);
		col.Green	= ((widthScale * point.X) / 2) + ((heightScale * point.Y) / 2);
		col.Blue	= ((widthScale * point.X) / 2) + ((heightScale * point.Y) / 2);

//----------------------------------------------------------------

		renderer->PreRender();
		renderer->Clear();

		spriteBatch->AddQuad(pos, col, uv, 1.0f);
		spriteBatch->Draw();
		spriteBatch->Purge();

		renderer->Present();
		renderer->PostRender();
	}
}

int main()
{
	TestApp app;
	app.Run();

	return 0;
}
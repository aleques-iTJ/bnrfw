#pragma once
#include "bnr.h"

#if defined (_DEBUG)
#pragma comment(lib, "Core-Debug-x86.lib")
#pragma comment(lib, "Renderer-Debug-x86.lib")
#pragma comment(lib, "RendererDX11-Debug-x86.lib")

#else
#pragma comment(lib, "Core-Release-x86.lib")
#pragma comment(lib, "Renderer-Release-x86.lib")
#pragma comment(lib, "RendererDX11-Release-x86.lib")
#endif

//----------------------------------------------------------------

FileSystem* g_FileSystem = new FileSystem;

class TestApp
{
public:
	TestApp ();
	~TestApp();

	void Run();

private:
	Window*	window;
};

TestApp::TestApp()
{
	window = new Window;
}

TestApp::~TestApp()
{
	if (window)	delete window;
}

//----------------------------------------------------------------

void TestApp::Run()
{
	

}

int main()
{
	TestApp app;
	app.Run();

	return 0;
}